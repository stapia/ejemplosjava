package prueba;

import java.awt.*;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.*;

public class MainDibujaImagen {
	
	public static void main(String[] args) throws IOException {
		JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Images", "jpg", "gif", "png");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showOpenDialog(null);
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
	    	// File f = chooser.getSelectedFile();
	    	System.out.println("You chose to open this file: " + chooser.getSelectedFile());
	    }
	    Image img = ImageIO.read(chooser.getSelectedFile());
	    System.out.println(img.toString());	
	}
}
