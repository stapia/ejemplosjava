
public class Student {
  private String name;
  private int id;
  public static int nextId = 1;
  public Student(String theName) {
    this.name = theName;
    id = nextId++;
  }
  
  public static StudentNode ponerDelante(StudentNode p, String name) {
    StudentNode nuevo = new StudentNode();
    nuevo.data = new Student(name);
    nuevo.next = p;
    return nuevo;
  }

  public static void main(String[] args) {
    StudentNode primero;
    primero = new StudentNode();
    primero.data = new Student("Alice");
    
    primero.next = new StudentNode();
    primero.data = new Student("Bob");
    
    primero = ponerDelante(primero, "John");
  }
}

class StudentNode {
  Student data;
  StudentNode next;
}