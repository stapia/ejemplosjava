package animacion;

import stdlib.StdDraw;
import tads.ArrayList;

public class Mundo {
	
	public static final double DIMENSION = 50.0;
	
	public static void main(String[] args) {
		StdDraw.setScale(-1.0, DIMENSION+1.0);
		StdDraw.enableDoubleBuffering();
		
		ArrayList<Pelota> lisPelotas = new ArrayList<>();
		for ( int i = 0; i < 3; ++i ) {
			Pelota p = new Pelota();
			lisPelotas.add(p);
		}

		for ( int i = 0; i < 3; ++i ) {
			Pelota p = new PelotaRoja();
			lisPelotas.add(p);
		}
		
		while ( true ) {
			StdDraw.clear();
			for ( int i = 0; i < lisPelotas.size(); ++i ) {
				Pelota p = lisPelotas.get(i); 
				p.muevete(0.05);
				p.dibujar();
			}
			StdDraw.show();
			StdDraw.pause(10);
		}
	}

}
