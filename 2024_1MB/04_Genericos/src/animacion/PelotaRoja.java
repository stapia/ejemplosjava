package animacion;

public class PelotaRoja extends Pelota {
	
	protected void comprobarRebote() {
		if ( cx - radio <= 0 || cx + radio >= Mundo.DIMENSION ) {
			vx = -vx * 1.2;
			vx = Math.abs(vx) > 8.0 ? vx / 8.0 : vx;
		} else if (  cy - radio <= 0 || cy + radio >= Mundo.DIMENSION  ) {
			vy = -vy * 1.2;
			vy = Math.abs(vy) > 8.0 ? vy / 8.0 : vy;
		}
	}
}
