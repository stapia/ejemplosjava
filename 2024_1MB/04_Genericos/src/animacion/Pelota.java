package animacion;

import stdlib.StdDraw;

public class Pelota {
	
	java.util.Random r = new java.util.Random();
	
	protected double radio = 2.0;
	
	protected double cx;
	protected double cy;
	
	protected double vx;
	protected double vy;
	
	public Pelota() {
		vx = 2.0;
		vy = 3.0;
		cx = radio + (Mundo.DIMENSION-2*radio) * r.nextDouble();
		cy = radio + (Mundo.DIMENSION-2*radio) * r.nextDouble();
		System.out.println(String.format("(x = %.2f; y = %.2f)", cx, cy));
	}
	
	public void muevete(double deltaT) {
		comprobarRebote();
		cx += this.vx * deltaT;
		this.cy += vy * deltaT;
	}
	
	protected void comprobarRebote() {
		if ( cx - radio <= 0 || cx + radio >= Mundo.DIMENSION ) {
			vx = -vx;
		} else if (  cy - radio <= 0 || cy + radio >= Mundo.DIMENSION  ) {
			vy = -vy;
		}
	}

	public void dibujar() {
		StdDraw.filledCircle(cx, cy, radio);
	}
}
