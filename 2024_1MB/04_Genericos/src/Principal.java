import java.util.Arrays;

import tads.ArrayList;

public class Principal {
	
	public static void main(String[] args) {
		
		String a = "Pan";
		String b = "Leche";
		
		String[] array = { a, b };
		System.out.println("Array: " + Arrays.toString(array));
		
		ArrayList<String> lista = new ArrayList<String>();
		lista.add(a);
		System.out.println(lista);
		
		lista.add(b);
		System.out.println(lista);

		lista.add(a);
		System.out.println(lista);
		
		// lista.add(5); El 5 no es un String
		
		for ( int i = 0; i < array.length; ++i ) {
			System.out.println("array[" + i + "] = " + array[i]);
		}
		
		for ( int i = 0; i < lista.size(); ++i ) {
			System.out.println("lista[" + i + "] = " + lista.get(i));
		}
		
		lista.remove(0);
		System.out.println("La lista despues de borrar: " + lista);
	}

}
