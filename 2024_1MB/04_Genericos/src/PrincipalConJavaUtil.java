import java.util.ArrayList;

public class PrincipalConJavaUtil {

	public static void main(String[] args) {
		
		String a = "Pan";
		String b = "Leche";
		
		ArrayList<String> lista = new ArrayList<String>();
		lista.add(a);
		System.out.println(lista);
		
		lista.add(b);
		System.out.println(lista);

		lista.add(a);
		System.out.println(lista);
		
		for ( int i = 0; i < lista.size(); ++i ) {
			System.out.println("lista[" + i + "] = " + lista.get(i));
		}
		
		lista.remove(0);
		System.out.println("La lista despues de borrar: " + lista);
	}

}
