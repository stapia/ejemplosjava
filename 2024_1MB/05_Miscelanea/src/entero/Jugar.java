package entero;

public class Jugar {
	
	public static void main(String[] args) {
		// No puedo: EnteroPositivo n1 = new EnteroPositivo(-2, true);
		
		EnteroPositivo n2 = EnteroPositivo.factoria(-4);
		System.out.println(n2);

		EnteroPositivo n3 = EnteroPositivo.factoria(4);
		System.out.println(n3);
		
		// NO puedo, es privado n3.numero = -3;
		
		EnteroPositivo n4 = EnteroPositivo.factoria(510);
		System.out.println(n4);
		
		try {
			
			EnteroPositivo noLLega = new EnteroPositivo(-5);
			System.out.println(noLLega);
			
		} catch ( IllegalArgumentException e ) {
			System.err.println("He recogido una excepcion que dice:");
			System.err.println(e.getMessage());
		}
		System.out.println("He seguido");	
		
		EnteroPositivo n5 = new EnteroPositivo(-5);
		System.out.println(n5);	
	}
}
