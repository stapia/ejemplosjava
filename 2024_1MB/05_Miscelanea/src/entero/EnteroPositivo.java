package entero;

public class EnteroPositivo {
	
	private int numero;
	
	public EnteroPositivo(int n) {
		if ( n <= 0 ) {
			throw new IllegalArgumentException("El n�mero debe ser positivo");
		}
		numero = n;
	}
	
	private EnteroPositivo(int n, boolean nada) {
		numero = n;
	}
	
	public static EnteroPositivo factoria(int n) {
		if ( n <= 0 ) {
			System.err.println("Error");
			return null; // No hay hay objeto <=> null
		}
		return new EnteroPositivo(n, true);
	}
	
	public String toString() {
		return String.format("El valor es: |%05d|%5d|%5x|", numero, numero, numero);
	}

}
