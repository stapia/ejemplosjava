import java.util.Arrays;

public class Principal {
	
	public static void main(String[] args) {
		In in = new In("datos/datos.txt");
		double[] valores = in.readAllDoubles();
		System.out.print(Arrays.toString(valores));
		StdDraw.setYscale(-10.2, 10.2);
		StdDraw.setXscale(-10.2, 10.2);
		StdDraw.setPenRadius(0.02);
		StdDraw.setPenColor(StdDraw.BLUE);
		StdDraw.point(valores[0], valores[2]);	
	}

}
