package animacion;

import stdlib.StdDraw;
import tads.ArrayList;

public class Mundo extends Object {
	
	public static final double DIMENSION = 50.0;
	
	public static void main(String[] args) {
		StdDraw.setScale(-1.0, DIMENSION+1.0);
		StdDraw.enableDoubleBuffering();
		
		ArrayList<Pelota> lisPelotas = new ArrayList<>();
		for ( int i = 0; i < 3; ++i ) {
			Pelota p = new Pelota(i*2, 2.0);
			lisPelotas.add(p);
		}

		for ( int i = 0; i < 3; ++i ) {
			Pelota p = new PelotaRoja(5.0, i*1.5+1);
			lisPelotas.add(p);
		}
		
		mostrarPelotasMal(lisPelotas);
		
		while ( true ) {
			StdDraw.clear();
			for ( int i = 0; i < lisPelotas.size(); ++i ) {
				Pelota p = lisPelotas.get(i); 
				p.muevete(0.05);
				p.dibujar();
				if ( p instanceof PelotaRoja ) {
					PelotaRoja r = (PelotaRoja)p; //cast
					r.saltar();
				}
				// p.saltar(); No se puede porque no est� en la clase base
			}
			StdDraw.show();
			StdDraw.pause(10);
		}
	}

	public static void mostrarPelotasMal(ArrayList<Pelota> lisPelotas) {
		String aux = "";
		for ( int i = 0; i < lisPelotas.size(); ++i ) {
			aux += lisPelotas.get(i).toString();
			aux += "\n";
		}
		System.out.println(aux);
	}

	public static void mostrarPelotasBien(ArrayList<Pelota> lisPelotas) {
		StringBuilder aux = new StringBuilder();
		for ( int i = 0; i < lisPelotas.size(); ++i ) {
			aux.append(lisPelotas.get(i).toString());
			aux.append("\n");
		}
		System.out.println(aux.toString());
	}

}
