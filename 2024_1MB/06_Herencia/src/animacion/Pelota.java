package animacion;

import stdlib.StdDraw;

public class Pelota {
	
	protected static java.util.Random r = new java.util.Random();
	
	protected double radio = 2.0;
	
	protected double cx;
	protected double cy;
	
	protected double vx;
	protected double vy;
	
	public Pelota(double vx, double vy) {
		this.vx = vx;
		this.vy = vy;
		cx = radio + (Mundo.DIMENSION-2*radio) * r.nextDouble();
		cy = radio + (Mundo.DIMENSION-2*radio) * r.nextDouble();
	}
	
	public void muevete(double deltaT) {
		comprobarRebote();
		cx += this.vx * deltaT;
		this.cy += vy * deltaT;
	}
	
	protected void comprobarRebote() {
		if ( cx - radio <= 0 ) {
			vx = Math.abs(vx);
		} else if (cx + radio >= Mundo.DIMENSION) {
			vx = -Math.abs(vx);
		} else if (cy - radio <= 0) {
			vy = Math.abs(vy);	
		} else if ( cy + radio >= Mundo.DIMENSION  ) {
			vy = -Math.abs(vy);
		}
	}

	public void dibujar() {
		StdDraw.filledCircle(cx, cy, radio);
	}
	
}
