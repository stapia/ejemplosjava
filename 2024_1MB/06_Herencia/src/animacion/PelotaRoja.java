package animacion;

import stdlib.StdDraw;

public class PelotaRoja extends Pelota {
	
	public PelotaRoja(double vx, double vy) {
		super(vx, vy); // Tiene que ser la primera sentencia		
	}
	
	@Override
	public void dibujar() {
		StdDraw.setPenColor(StdDraw.RED);
		super.dibujar();
		StdDraw.setPenColor(StdDraw.BLACK);
	}
	
	public void saltar() {
		if ( r.nextDouble() < 0.01 ) {
			cx += 5.0;	
		}
	}

	@Override
	public String toString() {
		String deObject = super.toString();
		return String.format("%s (x = %.2f; y = %.2f)", deObject, cx, cy);
	}
}
