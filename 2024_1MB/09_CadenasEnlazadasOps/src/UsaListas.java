
public class UsaListas {
	
	public static ListNode insertarDelante(ListNode prim, int dato) {
		ListNode aux = new ListNode(dato); // 1
		aux.next = prim; // 2
		// Esta mal poner: prim = aux; // 3  
		return aux; // 3
	}
	
	// PRE: que anterior no sea null y sea un nodo de la lista
	public static void insertarDetrasDe(ListNode anterior, int dato) {
		ListNode aux = new ListNode(dato); // 1
		aux.next = anterior.next; // 2
		anterior.next = aux; // 3
	}
	
	// PRE: Que la lista no este vacia
	public static void insertarAlFinal(ListNode prim, int dato) {
		ListNode ult = buscarElUltimo(prim);
		insertarDetrasDe(ult, dato);
	}
			
	public static ListNode buscarElUltimo(ListNode aux) {
		while ( aux != null && aux.next != null ) {
			// Avanzar
			aux = aux.next;
		}
		return aux;
	}

	public static ListNode buscarMayor10(ListNode aux) {
		while ( aux != null && aux.val <= 10 ) {
			// Avanzar
			aux = aux.next;
		}
		return aux;
	}
	
	
	public static void mostrar(ListNode aux) {
		while ( aux != null ) {
			// Hacer operacion
			System.out.print(aux.val);
			System.out.print(" ");
			// Avanzar
			aux = aux.next;
		}
		System.out.println();
	}
	
	public static ListNode eliminarPrimero(ListNode prim) {
		return prim.next; 
	}

	private static void eliminarDetrasDe(ListNode anterior) {
		anterior.next = anterior.next.next;
	}
	
	public static void main(String[] args) {
		ListNode primero = null;
		ListNode ultimoNull = buscarElUltimo(primero);
		System.out.println(ultimoNull);
		primero = insertarDelante(primero, 5); // 3
		mostrar(primero);
		primero = insertarDelante(primero, 3);
		mostrar(primero);
		ListNode mayor10 = buscarMayor10(primero);
		System.out.println("El mayor que 10: " + mayor10);
		primero = insertarDelante(primero, 20);
		mostrar(primero);
		mayor10 = buscarMayor10(primero);
		System.out.println("El mayor que 10: " + mayor10 + " val: " + mayor10.val);
		ListNode ultimo = buscarElUltimo(primero);
		System.out.println("Ultimo: " + ultimo + " val: " + ultimo.val);
		insertarDetrasDe(mayor10, 7);
		mostrar(primero);
		
		primero = eliminarPrimero(primero);
		mostrar(primero);
		primero = insertarDelante(primero, 30);
		mostrar(primero);
		
		mayor10 = buscarMayor10(primero);
		eliminarDetrasDe(mayor10);
		mostrar(primero);
	}


}
