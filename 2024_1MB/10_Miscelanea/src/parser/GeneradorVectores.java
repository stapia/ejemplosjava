package parser;

import java.util.Locale;
import java.util.Scanner;

public class GeneradorVectores {
	
	public static double[] generar(String texto) {
		double[] resultado = null;
		try ( Scanner sc = new Scanner(texto) ) { // try with resources
			sc.useLocale(Locale.US);
			int numDeElemento = sc.nextInt();
			resultado = new double[numDeElemento];
			for ( int i = 0; i < numDeElemento; ++i ) {
				resultado[i] = sc.nextDouble();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}
	
	public static void main(String[] args) {
		String text = "5 3.3 -6.2 4.1 -1.0 7.2";
		double[] array = GeneradorVectores.generar(text);
		for ( double num : array ) {
			System.out.print(num + " ");
		}
	}

}
