package fibonacci;

import java.util.Iterator;

public class FibonacciIterator implements Iterator<Integer>, Iterable<Integer> {
	
	private int an1 = 1;
	private int an2 = 1;
	
	private int i = 0;
	private int numeroDeElementos;
	
	public FibonacciIterator(int n) {
		numeroDeElementos = n;
	}
	
	@Override // Este viene de Iterable
	public Iterator<Integer> iterator() {
		return new FibonacciIterator(numeroDeElementos);
	}

	@Override // Este viene de Iterator
	public boolean hasNext() {
		return i < numeroDeElementos;
	}

	@Override // Este viene de Iterator
	public Integer next() {
		if ( i < 2 ) {
			++i;
			return 1;
		} 
		int an = an1 + an2;
		an2 = an1;
		an1 = an;
		++i;
		return an;
	}
		
	public static void main(String[] args) {
		// Enfoque procedimental
		int an_1 = 1;
		int an_2 = 1;
		int an;
		System.out.print(an_1 + " ");
		System.out.print(an_2 + " ");
		for ( int i = 0; i < 8; ++i ) {
			an = an_1 + an_2;
			System.out.print(an + " ");
			an_1 = an_2;
			an_2 = an;
		}
		// Enfoque POO
		System.out.println();
		
		FibonacciIterator iterator = new FibonacciIterator(10);
		while ( iterator.hasNext() ) {
			int asubn = iterator.next();
			System.out.print(asubn + " ");
		}

		// Foreach
		FibonacciIterator iterable = new FibonacciIterator(10);
		System.out.println();
		for (int asubn : iterable ) {
			System.out.print(asubn + " ");
		}
		
		// Los arrays (y otras muchas cosas) son iterables
		System.out.println();
		int[] array = { -3, 4, 6, 8 };
		for (int asubn : array ) {
			System.out.print(asubn + " ");
		}
	}
}
