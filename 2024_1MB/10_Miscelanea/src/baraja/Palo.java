package baraja;

import java.util.Random;

public class Palo {
	
	String unPalo = "Oros";
	
	enum Palos { OROS, ESPADAS, BASTOS, COPAS }
	
	Palos palo;
	
	private static final Random random = new Random(123); 
	
	Palo() {
		int n = random.nextInt(4);
		
		switch ( n ) {
		case 0:
			palo = Palos.OROS;
			break;
		case 1: 
			palo = Palos.BASTOS;
			break;
		case 2: 
			palo = Palos.COPAS;
			break;
		case 3: 
			palo = Palos.ESPADAS;
			break;
		}
	}
	
	void cambiarAleatorio() {
		// random = new Random(12); como es final no lo puedo reasignar
		random.setSeed(12); // Pero lo puedo modificar
	}
	
	boolean esOros() {
		return palo == Palos.OROS;
	}
	
	@Override
	public String toString() {
		return String.format("El palo es %s, numero: %d", palo.name(), palo.ordinal());
	}
	
	public static void main(String[] args) {
		for ( int i = 0; i < 10; ++i ) {
			Palo p = new Palo();
			System.out.println(p);
		}
		
		Palo q = new Palo();
		System.out.println("q: " + q + " esOro: " + q.esOros());
	}

}
