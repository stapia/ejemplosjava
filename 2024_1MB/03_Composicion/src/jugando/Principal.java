package jugando;

import geometria.Punto;
import geometria.Segmento;

public class Principal {
	
	public static void main(String[] args) {
		Segmento s1 = new Segmento();
		System.out.println(s1);
		s1.mostrar();
		
		Punto uno = new Punto();
		Punto dos = new Punto();
		Segmento s2 = new Segmento(uno, dos);
		System.out.println(s2);
		s2.mostrar();
		
		uno.x = 3;
		System.out.println(s2);
		s2.mostrar();
	}

}
