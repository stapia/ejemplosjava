package geometria;

public class Segmento {
	
	Punto a;
	Punto b;
	
	public Segmento() {
		a = new Punto();
		b = new Punto();
	}
	
	public Segmento(Punto p, Punto q) {
		a = p;
		b = q; 
	}
	
	public void mostrar() {
		System.out.println("[ " + a.x + ", " + a.y + ", " + b.x + ", " + b.y + "]");
	}
	
	public String toString() {
		return "[ " + a + ", " + b + " ]";
	}

}
