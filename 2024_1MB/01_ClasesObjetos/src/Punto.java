
public class Punto {
	
	// Constructor
	public Punto() {
		x = -1;
		y = -1;
	}
	
	// Este es otro constructor
	public Punto(double valor) {
		x = valor;
		y = valor;
	}
	
	// Esto es un atributo de instancia
	double x;
	double y;
	
	// Esto es un m�todo de instancia
	public void mover(double aX, double aY) {
		// aX toma el valor 2, aY toma el valor 3
		// y this toma el valor p
		this.x = aX;
		this.y = aY;
	}
	
	public void incrementar(double delta_x, double delta_y) {
		x += delta_x;
		y += delta_y;
	}
	
	public String toString() {
		return "( x: " + x + ", y: " + y + " )";
	}

}
