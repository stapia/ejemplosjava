
public class Principal {

	// Esto es un m�todo de clase
	public static void main(String[] args) {
		Principal.probandoUno();
	}
	
	// Esto es un m�todo de clase
	static void probandoUno() {
		// this.probandoUno(); MAL porque es static no existe this
		Punto p = new Punto();
		System.out.println(p);
		
		// Es de instancia tengo que poner la p delante
		p.incrementar(3.4, -5.6);
		// incrementar(2,3); MAL porque es de instancia y necesita un objeto delante
		System.out.println(p);
		
		Punto q; // = null;
		q = new Punto(7.0);
		q.incrementar(1.7, 6.7);
		System.out.println("El punto q es " + q.toString());
		
		// Con cuidado, hay gente a qui�n no le gusta
		p.x = 24.24;
		System.out.println(p);
		
		// Sustituye el `x` e `y` del punto, `p`, por 2 y 2.
		p.mover(2, 3);
		System.out.println("p: " + p);
		
		// Punto r = new Punto(4, 6); Esta mal porque no existe el constructor
	}

}
