
import java.util.Arrays;

public class PlayList {

    private Cancion[] array; 
    private int siguiente = 0;

    public PlayList(int size) {
        array = new Cancion[size];
    }

    public void add(Cancion c) {
        array[siguiente] = c;
        siguiente++;
    }

    public void remove(String t) {
        int index = 0; 
        boolean encontrado = false;
        while ( index < siguiente && ! encontrado ) {
            if ( array[index].titulo.equals(t) ) {
                encontrado = true;
            } else {
                index++;
            }
        }
        if ( encontrado ) {
            // array[index] = null; Esto deja null entre medias
            if ( siguiente == 1 ) {
                array[index] = null;
            } else {
                array[index] = array[siguiente-1];
            }
            siguiente--;
        }
    }

    public String toString() {
        return Arrays.toString(array);
    }
}
