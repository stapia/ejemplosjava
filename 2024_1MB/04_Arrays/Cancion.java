
public class Cancion {
  String titulo;
  public String interprete;
  private int valoracion;

  Cancion() {
    titulo = StdIn.readLine();
    interprete = StdIn.readLine();
    valoracion = Integer.parseInt(StdIn.readLine());
  }

  Cancion(String x, String y, int z) {
    titulo = x;
    interprete = y;
    valoracion = z;
  }

  public String toString() {
    return String.format("%s by %s (%d)", titulo, interprete, valoracion);
  }
}
