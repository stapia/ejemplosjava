public class ProcesarOrdenes {
  public static void main(String[] args) {
    jugarCrearCancion();
    procesar();
  }

  private static void procesar() {
    PlayList pl = new PlayList(20);
    // pl.siguiente = 3; Esto está prohibido por visibilidad
    String orden;
    while (!StdIn.isEmpty()) {
      orden = StdIn.readLine();
      if ( orden.equals("a") ) {
        pl.add(new Cancion());
      } else if ( orden.equals("r") ) {
        String t = StdIn.readLine();
        pl.remove(t);
      }
    }
    System.out.println(pl);
  }

  public static void jugarVisibilidad() {
    Cancion c = new Cancion("Wish you where here", "Pink Floyd", 4);
    System.out.println(c.titulo);
    System.out.println(c.interprete);
    // System.out.println(c.valoracion); NO puedo es private
  }

  public static void jugarCrearCancion() {
    System.out.println("Método jugarCrearCancion");
    Cancion c = new Cancion("Wish you where here", "Pink Floyd", 4);
    System.out.println(c);
    System.out.println("Fin de Método jugarCrearCancion");
  }
}
