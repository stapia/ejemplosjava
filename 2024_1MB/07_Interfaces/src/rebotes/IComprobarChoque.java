package rebotes;

public interface IComprobarChoque {
	
	boolean haChocado(double x, double y, double radio);

}
