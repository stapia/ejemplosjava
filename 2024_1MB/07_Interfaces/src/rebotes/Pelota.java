package rebotes;

import stdlib.StdDraw;

public class Pelota implements IDibujar, IMover, IRebota {
	
	final double deltaT = 0.05;
	
	double x;
	double y;
	
	double vx = 0.5;
	double vy = -0.1;
	
	double radio;
	
	public Pelota(double x, double y, double radio) {
		this.x = x;
		this.y = y;
		this.radio = radio;
	}
	
	@Override
	public void dibujar() {
		StdDraw.filledCircle(x, y, radio);
	}

	@Override
	public void mover() {
		x += vx * deltaT;
		y += vy * deltaT;
	}

	@Override
	public double coordX() {
		return x;
	}

	@Override
	public double coordY() {
		return y;
	}

	@Override
	public double radio() {
		return radio;
	}

	@Override
	public void rebotaEnX() {
		vx = -vx;
	}

	@Override
	public void rebotaEnY() {
		vy = -vy;
	}

}
