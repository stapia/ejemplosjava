package rebotes;

public interface IRebota {
	
	double coordX();
	double coordY();
	
	double radio();	
	
	void rebotaEnX();
	void rebotaEnY();

}
