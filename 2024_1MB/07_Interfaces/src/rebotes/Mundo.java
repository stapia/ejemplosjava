package rebotes;

import stdlib.StdDraw;
import tads.ArrayList;

public class Mundo {
	
	public static final double DIMENSION = 50.0;
	private static boolean fin = false;
	
	public static void main(String[] args) {
		StdDraw.setScale(-1.0, DIMENSION+1.0);
		StdDraw.enableDoubleBuffering();
		
		// NO se puede porque es una interface
		// IDibujar dibujar = new IDibujar();
		
		ArrayList<IDibujar> lista = new ArrayList<>();
				
		IDibujar dibujar = new Pelota(25, 25, 1);
		lista.add(dibujar);

		Pelota p = new Pelota(20, 35, 1);
		lista.add(p);

		// Cuidado. Debido a la optimización del bucle 
		// de comprobación de los choques, los rectángulos
		// debe ir los últimos
		lista.add(new Rectangulo(0, DIMENSION/2, DIMENSION/2, 1));
		lista.add(new Rectangulo(DIMENSION, DIMENSION/2, DIMENSION/2, 1));
		lista.add(new Rectangulo(DIMENSION/2, 0, 1, DIMENSION/2));
		lista.add(new Rectangulo(DIMENSION/2, DIMENSION, 1, DIMENSION /2));

		while ( ! fin ) {
			StdDraw.clear();
			for ( int i = 0; i < lista.size(); ++i ) {
				IDibujar d = lista.get(i); 
				d.dibujar();
				if ( d instanceof IMover ) {
					IMover m = (IMover)d;
					m.mover();
				}
			}
			
			for ( int i = 0; i < lista.size(); ++i ) {
				IDibujar d = lista.get(i);
				if ( d instanceof IRebota ) {
					IRebota rebota = (IRebota) d;
					double x = rebota.coordX();
					double y = rebota.coordY();
					double r = rebota.radio();
					
					for ( int j = i+1; j < lista.size(); ++j ) {
						IDibujar d2 = lista.get(j);
						if ( d2 instanceof IComprobarChoque) {
							IComprobarChoque choque = (IComprobarChoque)d2;
							if ( choque.haChocado(x, y, r) ) {
								rebota.rebotaEnX();
								rebota.rebotaEnY();
							}
						}					
					}					
				}
			}
			
			StdDraw.show();
			StdDraw.pause(10);
		}
	}
}
