package rebotes;

import stdlib.StdDraw;

public class Rectangulo implements IDibujar, IComprobarChoque {
	
	double x;
	double y;
	
	double halfHeight;
	double halfWidth;
	
	public Rectangulo(double x, double y, double hh, double hw) {
		this.x = x;
		this.y = y;
		halfHeight = hh;
		halfWidth = hw;
	}
	
	@Override
	public void dibujar() {
		StdDraw.filledRectangle(x, y, halfWidth, halfHeight);
	}

	@Override
	public boolean haChocado(double x, double y, double radio) {
		return (this.x - halfWidth - radio) <= x &&
				x <= (this.x + halfWidth + radio) &&
				(this.y - halfHeight - radio) <= y &&
				y <= (this.y + halfHeight + radio);
	}

}
