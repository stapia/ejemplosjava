# README #

En esta carpeta voy a poner ejemplos que voy a escribir antes de clase, como preparaci�n
previa, para que sirva de base a un desarrollo posterior, o como ejemplo a modificar. 

### Contenido

Son distintos proyectos de Eclipse con material en orden creciente de dificultad. 

Para utilizarlos sin Eclipse, sencillamente hay que compilar todos los paquetes
que se incluyan, identificar la clase con el "main" a utilizar y ejecutar. 

### Para actualizar

Desde Eclipse hay que entrar en Team y luego elegir el submenu *pull*



## Instruciones para bajar la librer�a TADS

### Explicaci�n

La carpeta 00_B_tads de este repositorio es un submodulo de Git.

Es decir, la carpeta 00_B_tads apunta a un otro repositorio
que hay que bajar de forma expl�cita. No basta con hacer
un pull de repositorio general. 

Y por esto hay que ejecutar un par de comandos adicionales.

### Comandos

Hay que abrir una terminal de git en cualquier carpeta del 
repositorio (git bash en Windows), una terminal cualquier
en GNU-Linux.

Y En la terminal ejecutar:

```
git submodule init
git submodule update
```

Si fuese necesario actualizar la versi�n de la propia carpeta, hay
que moverse a la carpeta 00_B_tads en la terminal y hacer:

```
git pull
```

Para bajar la �ltima versi�n del submodulo.
