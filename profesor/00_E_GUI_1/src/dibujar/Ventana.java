package dibujar;

import javax.swing.JFrame;

import geometria.Punto;

public class Ventana {

	private JFrame frame = new JFrame();
	private Lienzo lienzo;

	private Punto[] puntos;

	public Ventana() {
		lienzo = new Lienzo();
		puntos = new Punto[6];
		for (int i = 0; i < 5; ++i) {
			puntos[i] = new Punto(20 * (i+1), 30 * (i+1));
		}
		lienzo.setPuntos(puntos);
		puntos[5] = new Punto(100,100);
		lienzo.setBounds(10, 10, 590, 590);

		frame.add(lienzo);
		frame.setLayout(null);
		frame.setSize(620, 650);
		frame.setVisible(true);
	}
}
