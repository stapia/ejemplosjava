package geometria;

import stdlib.StdDraw;

public class Cuadrado implements IDibujar {
	
	Punto[] vertices;
	double lado;
	
	public Cuadrado(Punto topLeft, double lado) {
		this.lado = lado;
		vertices = new Punto[4];
		for ( int i = 0; i < 4; ++i ) {
			vertices[i] = new Punto();
		}
		vertices[0].x = topLeft.x;
		vertices[0].y = topLeft.y;
		vertices[1].x = topLeft.x + lado;
		vertices[1].y = topLeft.y;
		vertices[2].x = topLeft.x + lado;
		vertices[2].y = topLeft.y - lado;
		vertices[3].x = topLeft.x;
		vertices[3].y = topLeft.y - lado;		
	}
	
	public double calcularArea() {
		return lado*lado;
	}
	
	@Override
	public void dibujar() {
		for ( int i = 0; i < 3; ++i ) {
			StdDraw.line(vertices[i].x, vertices[i].y, vertices[i+1].x, vertices[i+1].y);
		}
		StdDraw.line(vertices[3].x, vertices[3].y, vertices[0].x, vertices[0].y);
	}

}
