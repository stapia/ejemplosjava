package geometria;

import stdlib.StdDraw;

public class Triangulo implements IDibujar {

	Punto[] vertices;
	
	public Triangulo(Punto topLeft, double lado) {
		vertices = new Punto[3];
		for ( int i = 0; i < 3; ++i ) {
			vertices[i] = new Punto();
		}
		vertices[0].x = topLeft.x;
		vertices[0].y = topLeft.y;
		vertices[1].x = topLeft.x + lado;
		vertices[1].y = topLeft.y;
		vertices[2].x = topLeft.x + lado;
		vertices[2].y = topLeft.y - lado;	
	}
	
	@Override
	public void dibujar() {
		for ( int i = 0; i < 2; ++i ) {
			StdDraw.line(vertices[i].x, vertices[i].y, vertices[i+1].x, vertices[i+1].y);
		}
		StdDraw.line(vertices[2].x, vertices[2].y, vertices[0].x, vertices[0].y);
	}

}
