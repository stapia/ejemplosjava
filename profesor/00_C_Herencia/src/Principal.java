
import geometria.IDibujar;
import geometria.Punto;
import geometria.Triangulo;
import stdlib.StdDraw;

public class Principal {
	
	public static void main(String[] args) {
		StdDraw.setScale(0.0, 100.0);
		StdDraw.setPenRadius(0.01);
		StdDraw.setPenColor(StdDraw.BLUE);
		Punto p = new Punto();
		/* p.x = 20; No se puede visibilidad es "paquete"
		p.y = 40; */
		p.set(20, 40); // Este es public 
		IDibujar c = new geometria.Cuadrado(p, 15);
		c.dibujar();
		// c.calcularArea(); No se puede
		Punto q = new Punto();
		q.set(70, 30);
		IDibujar  t = new Triangulo(q, 25);
		t.dibujar();
	}
	
	public static void test(String[] args) {
		StdDraw.setYscale(-1.2, 1.2);
		StdDraw.setPenColor(StdDraw.LIGHT_GRAY);
		StdDraw.line(0, 0, 1.0, 0);
		StdDraw.line(0, -0.02, 0.03, -0.02);
		StdDraw.line(1-0.03, -0.02, 1.0, -0.02);
		StdDraw.setPenRadius(0.02);
		StdDraw.setPenColor(StdDraw.BLUE);
		StdDraw.point(0.1, 0.5+0.1);
		StdDraw.point(0.1, -0.5-0.1);
		StdDraw.point(0.5, 0.0+0.1);
		StdDraw.point(0.5, -0.0-0.1);
		StdDraw.point(0.9, 1+0.1);
		StdDraw.point(0.9, -1-0.1);
	}
}
