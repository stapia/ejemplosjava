package puntos;

public class Punto {

	private double x;
	private double y;
	
	public Punto(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public void moverDelta(double ix, double iy) {
		x += ix;
		y += iy;
	}
	
	@Override
	public boolean equals(Object o) {
		boolean retorno = false;
		if ( o instanceof Punto ) {
			Punto p = (Punto) o;
			retorno = Math.abs(x-p.x) < 1e-13 && Math.abs(y-p.y) < 1e-13;	
		}
		return retorno;
	}
	
	@Override
	public String toString() {
		String strObj = super.toString();
		return "[ " + x + ", " + y + " ]; " + strObj;
	}
}
