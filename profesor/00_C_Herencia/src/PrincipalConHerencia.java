
import conherencia.*;
import stdlib.StdDraw;

public class PrincipalConHerencia {
	
	public static void main(String[] args) {
		StdDraw.setScale(0.0, 100.0);
		StdDraw.setPenRadius(0.01);
		StdDraw.setPenColor(StdDraw.RED);
		Punto p = new Punto();
		p.set(20, 40);
		Cuadrado c1 = new Cuadrado(p, 15);
		c1.dibujar();
		double perimetro = c1.calcularPerimetro();
		System.out.println(perimetro);
		p.set(50, 50);
		Figura c2 = new Cuadrado(p, 10);
		c2.dibujar();
		// c2.calcularPerimetro(); No puedo
		double area = c2.calcularArea();
		System.out.println("El area de c2 es " + area);
		// c2.calcularPerimetro(); No se puede
		Punto q = new Punto();
		q.set(70, 30);
		Figura  t = new Triangulo(q, 25);
		t.dibujar();
		System.out.println(t.calcularArea());
	}
	
}
