import conherencia.*;
import stdlib.StdDraw;

public class PrincipalConPolimorfismo {
	
	public static void main(String[] args) {
		StdDraw.setScale(0.0, 100.0);
		StdDraw.setPenRadius(0.01);
		StdDraw.setPenColor(StdDraw.BLUE);
		Punto p = new Punto();
		p.set(20, 40);
		Punto q = new Punto();
		q.set(20, 90);
		Figura[] figuras = new Figura[4];
		figuras[0] = new Cuadrado(p, 15);
		p.set(50, 50);
		figuras[1] = new Cuadrado(p, 10);
		figuras[2] = new Triangulo(q, 25);
		figuras[3] = new TrianguloFino(q, 35);
		
		for (int i = 0; i < figuras.length; ++i) {
			figuras[i].dibujar();
			System.out.println(figuras[i].calcularArea());
		}
	}

}
