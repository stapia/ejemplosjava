package conherencia;

import stdlib.StdDraw;

public class TrianguloFino extends Triangulo {
	
	double grosor;
	
	public TrianguloFino(Punto topLeft, double lado, double grosor) {
		// int hola = 0; // No puedo
		super(topLeft, lado); // Constructor del Triangulo
		this.grosor = grosor;
	}
	
	public TrianguloFino(Punto topLeft, double lado) {
		// int hola = 0; // No puedo
		super(topLeft, lado); // Constructor del Triangulo
		this.grosor = 0.005;
	}

	@Override
	public void dibujar() {
		StdDraw.setPenRadius(grosor);
		super.dibujar();
		StdDraw.setPenRadius(0.01);
	}
}
