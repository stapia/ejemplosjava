package conherencia;

public class Cuadrado extends Figura {
	
	double lado; 
	
	public Cuadrado(Punto topLeft, double lado) {
		this.lado = lado;
		vertices = new Punto[4];
		for ( int i = 0; i < 4; ++i ) {
			vertices[i] = new Punto();
		}
		vertices[0].x = topLeft.x;
		vertices[0].y = topLeft.y;
		vertices[1].x = topLeft.x + lado;
		vertices[1].y = topLeft.y;
		vertices[2].x = topLeft.x + lado;
		vertices[2].y = topLeft.y - lado;
		vertices[3].x = topLeft.x;
		vertices[3].y = topLeft.y - lado;		
	}
	
	public double calcularPerimetro() {
		return 4*lado;
	}
	
	@Override
	public double calcularArea() {
		return lado*lado;
	}
}
