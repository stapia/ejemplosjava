package conherencia;

import stdlib.StdDraw;

public class Figura implements IDibujar {

	Punto[] vertices; 
	
	public double calcularArea() {
		return 0.0;
	}
	
	@Override
	public void dibujar() {
		for ( int i = 0; i < vertices.length-1; ++i ) {
			StdDraw.line(vertices[i].x, vertices[i].y, vertices[i+1].x, vertices[i+1].y);
		}
		StdDraw.line(vertices[vertices.length-1].x, vertices[vertices.length-1].y, vertices[0].x, vertices[0].y);
	}
	
}
