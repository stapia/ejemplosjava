package conherencia;

import stdlib.StdDraw;

public class Triangulo extends Figura {
	
	double lado; 
	
	public Triangulo(Punto topLeft, double lado) {
		this.lado = lado;
		vertices = new Punto[3];
		for ( int i = 0; i < 3; ++i ) {
			vertices[i] = new Punto();
		}
		vertices[0].x = topLeft.x;
		vertices[0].y = topLeft.y;
		vertices[1].x = topLeft.x + lado;
		vertices[1].y = topLeft.y;
		vertices[2].x = topLeft.x + lado;
		vertices[2].y = topLeft.y - lado;	
	}
	
	@Override
	public double calcularArea() {
		return lado * lado / 2.0;
	}
	
	@Override
	public void dibujar() {
		StdDraw.setPenColor(StdDraw.RED);
		/* Repetido for ( int i = 0; i < vertices.length-1; ++i ) {
			StdDraw.line(vertices[i].x, vertices[i].y, vertices[i+1].x, vertices[i+1].y);
		}
		StdDraw.line(vertices[vertices.length-1].x, vertices[vertices.length-1].y, vertices[0].x, vertices[0].y);
		Deberia llamar al dibujar de Figura */
		super.dibujar(); // Esto lo llama
		StdDraw.setPenColor(StdDraw.BLUE);
	}

}
