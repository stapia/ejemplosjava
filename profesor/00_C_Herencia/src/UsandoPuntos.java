
import puntos.*;

public class UsandoPuntos {
	
	public static void main(String[] args) {
		Punto a = new Punto(20.3, 10.1);
		System.out.println("a: " + a.toString());
		Punto b = new Punto(20.3, 10.1);
		System.out.println("b: " + b);
		Punto c = a;
		System.out.println("a == b " + (a == b));
		System.out.println("a == b " + (a == c));
		
		// Trabajar con equals y con String
		System.out.println("a.equals b " + a.equals(b));
		System.out.println("a.equals c " + a.equals(c));
		
		String str1 = "Hola";
		String str2 = "Hola";
		System.out.println("str1 == str2 " + (str1 == str2)); // ERROR
		System.out.println("str1.equals str2 " + str1.equals(str2));
		
		str2 = str1.replace('o', 'a');
		System.out.println(str1);
		System.out.println(str2);
	}
}
