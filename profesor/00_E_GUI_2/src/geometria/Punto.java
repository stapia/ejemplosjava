package geometria;

import java.awt.Graphics;

import dibujar.IDibujar;

public class Punto implements IDibujar {
	
	private static final int POINT_SIZE = 8; 
	
	private int x;
	private int y;
	
	public Punto(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public void dibujar(Graphics g) {
		int cx = getX() - POINT_SIZE / 2;
		int cy = getY() - POINT_SIZE / 2;
		g.fillOval(cx, cy, POINT_SIZE, POINT_SIZE);		
	}

}
