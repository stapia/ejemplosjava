package dibujar;

import javax.swing.*;
import java.awt.*;
import java.util.*;

@SuppressWarnings("serial")
public class Lienzo extends JComponent {

	int width = 800; 
	int height = 550;
	Color color = Color.black;
	
	private ArrayList<IDibujar> dibujables = new ArrayList<>();
	
	public void add(IDibujar d) {
		dibujables.add(d);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		g.setColor(color);
		g.fillRect(0, 0, width, height);
		
		g.setColor(Color.white);
		for ( IDibujar d : dibujables ) {
			d.dibujar(g);
		}
	}	
}
