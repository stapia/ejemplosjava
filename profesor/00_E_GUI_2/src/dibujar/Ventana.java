package dibujar;

import java.awt.event.*;
import javax.swing.*;
import geometria.Punto;

public class Ventana implements MouseListener {

	private JFrame frame = new JFrame();
	private Lienzo lienzo;
	private JLabel label;
	
	public Ventana() {
		frame.setLayout(null);
		frame.setSize(1000, 600);
		
		lienzo = new Lienzo();
		for (int i = 0; i < 10; ++i) {
			lienzo.add(new Punto(50 * i, 50 * i));
		}
		lienzo.setBounds(10, 40, 800, 550);
		lienzo.setFocusable(true);
		frame.add(lienzo);
		
		label = new JLabel("Hola mundo");
		label.setBounds(10, 10, 800, 35);
		frame.add(label);
		
		frame.addMouseListener(this);
		
		frame.setVisible(true);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		label.setText(e.toString());
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
	}
}
