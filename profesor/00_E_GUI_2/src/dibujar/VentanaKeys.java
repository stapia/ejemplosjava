package dibujar;

import java.awt.event.*;
import javax.swing.*;
import geometria.Punto;

public class VentanaKeys implements KeyListener, MouseListener {

	private JFrame frame = new JFrame();
	private Lienzo lienzo;
	private JLabel label;
	
	public VentanaKeys() {
		frame.addKeyListener(this);
		frame.addMouseListener(this);
		
		frame.setLayout(null);
		frame.setSize(1440, 870);
		
		lienzo = new Lienzo();
		for (int i = 0; i < 10; ++i) {
			lienzo.add(new Punto(50 * i, 50 * i));
		}
		lienzo.setBounds(10, 40, 1400, 700);
		frame.add(lienzo);
		
		label = new JLabel("Hola mundo");
		label.setBounds(10, 10, 1400, 35);
		frame.add(label);
		
		frame.setVisible(true);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		label.setText(e.toString());
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		label.setText(e.toString());
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
