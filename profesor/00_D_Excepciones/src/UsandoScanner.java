import java.util.*;

public class UsandoScanner {
	
	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			int suma = 0; 
			while ( suma < 100 ) {
			// while ( sc.hasNextInt() ) {
				int num = sc.nextInt();
				suma += num;
			}
			System.out.println(suma);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public static void mainConFinally(String[] args) {
		Scanner sc = null;
		try {
			sc = new Scanner(System.in);
			int suma = 0; 
			while ( sc.hasNextInt() ) { // Esto no lanza
			// while ( suma < 1000 ) { // Este lanza
				int num = sc.nextInt();
				suma += num;
			}
			System.out.print("Resultado es ");
			System.out.println(suma);
		} catch (InputMismatchException e) {
			System.out.println("Encontre una exception de Input");
		} catch (Exception e) {
			System.out.println("Encontre una exception");
			e.printStackTrace();
		} finally {
			System.out.println("Estoy en finally");
			if ( sc != null ) sc.close();
		}
	}
	
	public static void mainSinExcepciones(String[] args) {
		Scanner sc = new Scanner(System.in);
		int suma = 0; 
		while ( sc.hasNextInt() ) {
			int num = sc.nextInt();
			suma += num;
		}
		sc.close();
		System.out.println(suma);
	}

}
