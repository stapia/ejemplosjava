package archivos;

import java.io.*;
import java.util.*;

public class Leyendo {
	
	public static void main(String[] args) {
		File f = new File("datos.txt");
		try (Scanner sc = new Scanner(f)) {
			LeerArchivo a = new LeerArchivo(sc);
			System.out.println(a);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
