package archivos;

import java.util.*;

public class LeerArchivo {
	
	int[] datos;
	
	public LeerArchivo(Scanner sc) {
		int tam = sc.nextInt();
		datos = new int[tam];
		for ( int i = 0; i < tam; ++i ) {
			datos[i] = sc.nextInt();
		}
	}
	
	@Override
	public String toString() {
		return Arrays.toString(datos);
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner("4 1 34 9 2 45");
		LeerArchivo a = new LeerArchivo(sc);
		System.out.println(a);
	}

}
