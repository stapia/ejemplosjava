import algebra.Vector;

public class Principal {
	
	public static void main(String[] args) {
		Vector v1 = new Vector(1, 3, 4);
		Vector v2 = new Vector(2, 1, 3);
		Vector v3 = new Vector(2, 1);
		
		Vector res1 = Vector.suma(v1, v2);
		System.out.println(res1);
		try {
			Vector res2 = Vector.suma(v3, v2);
			System.err.println(res2);
		} catch (IllegalArgumentException e) {
			System.err.println("Se ha producido un excepcion");
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		System.err.println("Sigo con el v1 y v3");
		Vector res3 = Vector.suma(v1, v3);
		System.err.println(res3);
	}

}
