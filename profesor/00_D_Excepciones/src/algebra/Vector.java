package algebra;

public class Vector {
	
	private double[] v;
	
	private Vector() {
	}
	
	public Vector(double a, double b) {
		v = new double[2];
		v[0] = a;
		v[1] = b;
	}
	
	public Vector(double a, double b, double c) {
		v = new double[3];
		v[0] = a;
		v[1] = b;
		v[2] = c;
	}
	
	// El resultado "debe" ser un nuevo Vector
	// PRECONDICION: a y b tienen la misma dimension
	// throws IllegalArgumentException cuando no se cumple
	public static Vector suma(Vector a, Vector b) {
		if ( a.v.length != b.v.length) {
			// lanzo no gestionada
			throw new IllegalArgumentException("a y b no tienen la misma dimension"); 
		}
		System.out.println("Me pongo a sumar");
		Vector result = new Vector();
		result.v = new double[a.v.length];
		for (int i = 0; i < a.v.length; ++i) {
			result.v[i] = a.v[i] + b.v[i];
		}
		return result;
	}
	
	// PRECONDICION: a y b tienen la misma dimension
	// throws DimDistintaException cuando no se cumple
	public double prodEscalar(Vector b) throws DimDistintaException {
		if ( this.v.length != b.v.length) {
			// lanzo gestionada
			throw new DimDistintaException(); 
		}
		int suma = 0;
		for (int i = 0; i < v.length; ++i ) {
			suma += v[i] * b.v[i];
		}
		return suma;
	}
	
	public double[] prodEscalar(Vector[] vv) throws DimDistintaException {
		double[] res = new double[vv.length];
		for ( int i = 0; i < vv.length; ++i ) {
			res[i] = this.prodEscalar(vv[i]);
		}
		return res;
	}

}
