package algebra;

public class DimDistintaException extends Exception {

	public DimDistintaException() {
		super("Las dimensiones de los vectores son distintas");
	}
}
