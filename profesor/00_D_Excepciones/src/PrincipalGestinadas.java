import algebra.*;

public class PrincipalGestinadas {
	public static void main(String[] args) {
		Vector v1 = new Vector(1, 3, 4);
		Vector v2 = new Vector(2, 1, 3);
		Vector v3 = new Vector(2, 1);
		
		try {
			double r1 = v1.prodEscalar(v2);
			System.out.println("r1: " + r1);
		} catch (DimDistintaException e) {
			System.out.println("Excepcion al hacer v1.prodEscalar(v2)");
			e.printStackTrace();
		}
		
		try {
			double r2 = v1.prodEscalar(v3);
			System.out.println("r2: " + r2);
		} catch (DimDistintaException e) {
			System.out.println("Excepcion al hacer v1.prodEscalar(v3)");
			System.out.println(e.getMessage());
			// e.printStackTrace();
		}
		System.out.println("Sigo en main");
	}
}
