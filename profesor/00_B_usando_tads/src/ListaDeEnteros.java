import tads.*;

public class ListaDeEnteros {
	
	public static void main(String[] args) {
		
		// LinkedList<Integer> list = ... */
		// IList<Integer> list = new LinkedList<>();
		IList<Integer> list = new ArrayList<>();
		
		System.out.println("Size: " + list.size());

		for ( int i = 0; i < 10; ++i ) {
			list.add(0, i*5);
		}
		System.out.println("Size: " + list.size());
		
		System.out.println("El elemento 1 es " + list.get(1));
		
		list.add(1, 99);
		
		list.set(3, 999);

		System.out.println(list);
		
		for (int i = 1; i < list.size()-1; ++i ) {
			System.out.println(list.get(i));
		}
	}

}
