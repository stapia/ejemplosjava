package pelotas1;

import java.util.Random;

public class Campo {
	
	public static final Random r = new Random();
	
	private static final double DELTA_TIEMPO = 0.005;
	public static final double DIMENSION = 100.0;
	
	private PelotaRoja[] pelotasRojas;
	
	private int numeroDeAzules;
	private PelotaAzul[] pelotasAzul;
	
	public int contador;
	
	public Campo() {
		pelotasRojas = new PelotaRoja[3];
		pelotasRojas[0] = new PelotaRoja(10,80);
		pelotasRojas[1] = new PelotaRoja(50,50);
		pelotasRojas[2] = new PelotaRoja(40,10);
		
		pelotasAzul = new PelotaAzul[30];
		pelotasAzul[0] = new PelotaAzul(15,80);
		pelotasAzul[1] = new PelotaAzul(55,50);
		pelotasAzul[2] = new PelotaAzul(45,10);
		numeroDeAzules = 3;
		
		contador = 0;
	}
	
	/**
	 * A�ade una pelota azul a este campo. 
	 * @param azul
	 */
	public void addPelotaAzul(PelotaAzul azul) {
		pelotasAzul[numeroDeAzules] = azul;
		++numeroDeAzules;
	}
	
	public void tick() {
		for ( int i = 0; i < pelotasRojas.length; ++i ) {
			pelotasRojas[i].mover(DELTA_TIEMPO);
		}
		for ( int i = 0; i < numeroDeAzules; ++i ) {
			pelotasAzul[i].mover(DELTA_TIEMPO);
		}
		if ( contador % 500 == 1) {
			PelotaRoja.vx = 1.0 + r.nextInt(10);
			PelotaRoja.vy = 1.0 + r.nextInt(10);
		}
		++contador;
	}
	
	public void dibujar() {
		for ( int i = 0; i < pelotasRojas.length; ++i ) {
			pelotasRojas[i].dibujar();
		}
		// �Por qu� no vale pelotasAzules.length?
		//for ( int i = 0; i < pelotasAzul.length; ++i ) {
		for ( int i = 0; i < numeroDeAzules; ++i ) {
			pelotasAzul[i].dibujar();
		}
	}

}
