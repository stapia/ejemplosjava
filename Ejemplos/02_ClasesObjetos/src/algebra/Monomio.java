package algebra;

public class Monomio {
	
	/* Atributos de instancia */  
	double coeficiente;
	int exponente;

	/* Atributo de clase */
	static char variableIndependiente = 'X';
	
	/* M�todo Constructor */
	public Monomio(double c, int e) {
		coeficiente = c;
		if ( e < 0 ) {
			System.out.println("error, coeficiente negativo");
			throw new IllegalArgumentException("error, coeficiente negativo");
		}
		exponente = e;
	}
	
	/* M�todo de clase.
	 * Toma dos par�metros de tipo Mnomio
	 * Multiplica y devuelve el resultado */
	static Monomio multiplicar(Monomio m1, Monomio m2) {
		double coef = m1.coeficiente * m2.coeficiente;
		int exp = m1.exponente + m2.exponente;
		return new Monomio(coef, exp);
	}
	
	/* M�todo de instancia (instancia es sinonimo de objeto)
	 * Falta un parametro? Pues no, ese par�metro es this 
	 * */
	Monomio multiplicar(Monomio m2) { 
		return new Monomio(this.coeficiente * m2.coeficiente, this.exponente + m2.exponente);		
	}
	
	public String toString() {
		return "Monomio " + coeficiente + " * " + variableIndependiente + "^" + exponente;
	}
	
	boolean equalsTo(Monomio otro) {
		return coeficiente == otro.coeficiente && 
				this.exponente == otro.exponente;
	}
	
	public static void main(String[] args) {
		Monomio a = new Monomio(2.0,7);
		System.out.println("a: " + a);
		/* Lanza exception: Monomio b = new Monomio(2.0,-7); */
		Monomio b = new Monomio(2.0,7);
		System.out.println("b: " + b);
		
		System.out.println(a == b);
		System.out.println(a.equalsTo(b));
			
		Monomio m = Monomio.multiplicar(a, b);
		System.out.println(m);
	}

}
