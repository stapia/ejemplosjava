package pelotas3;

import java.util.Random;

import stdlib.StdDraw;

public class PelotaAzul extends Pelota implements SeMueve {
	
	protected static final Random r = new Random();
	
	protected double vx;
	protected double vy;
	
	public PelotaAzul(double x, double y) {
		this.x = x;
		this.y = y;
		vx = r.nextDouble() * 7 - 2;
		vy = r.nextDouble() * 7 - 2;
		radio = 3.0;
		color = StdDraw.BLUE;
	}
	
	@Override
	public void mover(double deltaTiempo) {
		x = x + vx * deltaTiempo;
		y = y + vy * deltaTiempo;
		if ( x >= Campo.DIMENSION || x <= 0) {
			vx = -vx;
		}
		if ( y > Campo.DIMENSION || y <= 0 ) {
			vy = -vy;
		}
	}

}
