package pelotas3;

import java.util.Random;

public class Campo {
	
	public static final Random r = new Random();
	
	private static final double DELTA_TIEMPO = 0.005;
	public static final double DIMENSION = 100.0;
	
	private Pelota[] pelotas;
	int contador;
	
	public Campo() {
		pelotas = new Pelota[14];
		pelotas[0] = new PelotaGris(10,80);
		pelotas[1] = new PelotaGris(50,50);
		pelotas[2] = new PelotaGris(40,10);
		pelotas[3] = new PelotaAzul(15,80);
		pelotas[4] = new PelotaAzul(55,50);
		pelotas[5] = new PelotaAzul(45,10);
		pelotas[6] = new PelotaRoja(10,10);
		pelotas[7] = new PelotaRoja(25,30);
		pelotas[8] = new PelotaRoja(45,70);
		pelotas[9] = new PelotaRoja(90,30);
		pelotas[10] = new PelotaRoja(10,50);
		pelotas[11] = new PelotaRoja(25,50);
		pelotas[12] = new PelotaRoja(5,70);
		pelotas[13] = new PelotaRoja(90,10);
		contador = 0;
	}
	
	public void tick() {
		for ( int i = 0; i < pelotas.length; ++i ) {
			if ( pelotas[i] instanceof SeMueve ) {
				SeMueve seMueve = (SeMueve) pelotas[i];
				seMueve.mover(DELTA_TIEMPO);
			}
			for ( int j = i + 1; j < pelotas.length; ++j ) {
				pelotas[i].choca(pelotas[j]);
			}
		}
		
		++contador;
	}
	
	public void dibujar() {
		for ( int i = 0; i < pelotas.length; ++i ) {
			pelotas[i].dibujar();
		}
	}

}
