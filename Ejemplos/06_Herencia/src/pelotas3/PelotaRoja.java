package pelotas3;

import stdlib.StdDraw;

public class PelotaRoja extends PelotaAzul implements Rebota {

	public PelotaRoja(double x, double y) {
		super(x, y);
		color = StdDraw.RED;
		radio = 1.5;
	}

	@Override
	public void rebota() {
		vx = - vx;
		vy = - vy;
	}
}
