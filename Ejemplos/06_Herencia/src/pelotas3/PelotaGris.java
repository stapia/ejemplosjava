package pelotas3;

import stdlib.StdDraw;

public class PelotaGris extends Pelota {
	
	public PelotaGris(double x, double y) {
		this.x = x;
		this.y = y;
		radio = 5.0;
		color = StdDraw.DARK_GRAY;
	}
	
}
