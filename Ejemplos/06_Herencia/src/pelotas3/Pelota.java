package pelotas3;

import java.awt.Color;
import java.math.MathContext;

import stdlib.StdDraw;

public class Pelota {
	
	protected double x;
	protected double y;
	protected double radio;
	protected Color color;
	
	public void dibujar() {
		StdDraw.setPenColor(color);
		StdDraw.filledCircle(x, y, radio);
	}

	
	public double[] posicion() {
		return new double[] { x, y };
	}
	
	public void choca(Pelota otra) {
		double d2 = Math.pow(x-otra.x, 2) + Math.pow(y-otra.y, 2);
		double radio2 = Math.pow(radio + otra.radio, 2);
		if ( d2 < radio2 ) {
			if ( this instanceof Rebota ) {
				Rebota r = (Rebota)this;
				r.rebota();
			}
			if ( otra instanceof Rebota ) {
				Rebota r = (Rebota)otra;
				r.rebota();				
			}
		}
	}

}
