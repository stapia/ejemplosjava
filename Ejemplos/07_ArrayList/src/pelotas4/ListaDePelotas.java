package pelotas4;

import java.util.Arrays;

public class ListaDePelotas {
	
	private static final int MAX_SIZE = 32;
	private int numeroDePelotas = 0;
	private int arrayLength = 8;
	
	private Pelota[] array = new Pelota[arrayLength];

	public boolean add(Pelota pelota) {
		if ( numeroDePelotas == MAX_SIZE ) {
			return false;
		}
			
		if ( numeroDePelotas + 1 > arrayLength ) {
			// Tama�o insuficiente, se hace crecer el array:
			arrayLength *= 2;
			array = Arrays.copyOf(array, arrayLength);
		}
		
		array[numeroDePelotas] = pelota;
		++numeroDePelotas;
		return true;
	}

	public int size() {
		return numeroDePelotas;
	}

	public Pelota get(int i) {
		return array[i];
	}

}
