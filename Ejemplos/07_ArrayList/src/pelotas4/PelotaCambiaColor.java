package pelotas4;

import java.util.Random;
import stdlib.StdDraw;

public class PelotaCambiaColor extends Pelota {
	protected static final Random r = new Random();
	
	protected double vx;
	protected double vy;
	
	public PelotaCambiaColor(double x, double y) {
		this.x = x;
		this.y = y;
		vx = 4 * (1-r.nextDouble());
		vy = 4 * (1-r.nextDouble());
		radio = 4.0;
		color = StdDraw.BLUE;
	}
	

	@Override
	public void tick() {
		x = x + vx * Campo.DELTA_TIEMPO;
		y = y + vy * Campo.DELTA_TIEMPO;
		if ( x >= Campo.DIMENSION || x <= 0) {
			vx = -vx;
		}
		if ( y > Campo.DIMENSION || y <= 0 ) {
			vy = -vy;
		}		
	}

	@Override
	public void reset() {
		color = StdDraw.BLUE;
	}

	@Override
	public void accion() {
		color = StdDraw.RED;
	}

}
