package pelotas4;

public interface CreaPelota {
	
	/**
	 * Crea una nueva pelota y la a�ade a pelotas
	 * 
	 * @param pelotas, la lista donde se a�ade la nueva pelota.
	 */
	void creaPelota(ListaDePelotas pelotas);

}
