package pelotas4;

public class Campo {
	
	public static final double DIMENSION = 100.0;
	public static final double DELTA_TIEMPO = 0.005;
	
	private ListaDePelotas pelotas;
	int contador;
	
	public Campo() {
		pelotas = new ListaDePelotas();
		pelotas.add(new PelotaCuadrada(10,80));
		pelotas.add(new PelotaCuadrada(20,30));
		pelotas.add(new PelotaCuadrada(30,10));
		pelotas.add(new PelotaCuadrada(40,50));
		pelotas.add(new PelotaCuadrada(50,10));
		pelotas.add(new PelotaCuadrada(60,60));
		pelotas.add(new PelotaCuadrada(70,30));
		pelotas.add(new PelotaCuadrada(80,50));
		pelotas.add(new PelotaCuadrada(90,70));
		pelotas.add(new PelotaCambiaColor(40,60));
		pelotas.add(new PelotaCambiaColor(50,50));
		pelotas.add(new PelotaCambiaColor(20,30));
		pelotas.add(new PelotaCambiaColor(25,90));
		pelotas.add(new PelotaGeneradora(50,50));
		contador = 0;
	}
	
	public void tick() {
		for ( int i = 0; i < pelotas.size(); ++i ) {
			pelotas.get(i).reset();
		}

		for ( int i = 0; i < pelotas.size(); ++i ) {
			Pelota pelota = pelotas.get(i);
			for ( int j = 0; j < pelotas.size(); ++j ) {
				Pelota otra = pelotas.get(j);
				if ( i != j && pelota.estaDentroDe(otra) ) {
					pelota.accion();
				}
			}
		}

		for ( int i = 0; i < pelotas.size(); ++i ) {
			Pelota pelota = pelotas.get(i);
			pelota.tick();
			if ( pelota instanceof CreaPelota ) {
				CreaPelota c = (CreaPelota) pelota;
				c.creaPelota(pelotas);
			}
		}
	}
	
	public void dibujar() {
		for ( int i = 0; i < pelotas.size(); ++i ) {
			pelotas.get(i).dibujar();
		}
	}

}
