package pelotas4;

import java.awt.Color;
import stdlib.StdDraw;

public abstract class Pelota {
	
	protected double x;
	protected double y;
	protected double radio;
	protected Color color;
	
	public abstract void tick();
	public abstract void reset();
	public abstract void accion();
	
	/**
	 * Comprueba si esta pelota est� dentro de otra
	 * 
	 * Se considera que esta pelota est� dentro de la otra pelota si 
	 * el centro de �sta es un punto interior a la otra.  
	 * @param x, coord x del punto
	 * @param y, coord y del punto
	 * @return verdadero si el punto est� dentro, falso en caso contrario
	 */
	public boolean estaDentroDe(Pelota otra) {
		return otra.esUnPuntoInterno(this.x, this.y);
	}

	/**
	 * Comprueba si el punto dado por sus coordenadas est� dentro de la pelota
	 * 
	 * @param x, coord x del punto
	 * @param y, coord y del punto
	 * @return verdadero si el punto est� dentro, falso en caso contrario
	 */
	public boolean esUnPuntoInterno(double x, double y) {
		return Math.pow(x-this.x,2) + Math.pow(y-this.y,2)  < radio*radio; 
	}

	/**
	 * Dibuja esta pelota como un circulo con relleno
	 */
	public void dibujar() {
		StdDraw.setPenColor(color);
		StdDraw.filledCircle(x, y, radio);
	}

}
