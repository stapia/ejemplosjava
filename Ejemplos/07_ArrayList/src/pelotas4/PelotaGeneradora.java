package pelotas4;

import java.awt.Color;

import stdlib.StdDraw;

public class PelotaGeneradora extends PelotaCambiaColor implements CreaPelota {
	
	protected Color lastColor;
	
	public PelotaGeneradora(double x, double y) {
		super(x, y);
		radio = 4;
		lastColor = StdDraw.BLUE;
	}

	@Override
	public void creaPelota(ListaDePelotas pelotas) {
		if ( color.equals(StdDraw.RED) && !color.equals(lastColor) ) {
			pelotas.add(new PelotaGeneradora(10, 10));
			lastColor = color;
		}
	}

	@Override
	public void dibujar() {
		StdDraw.setPenColor(StdDraw.MAGENTA);
		StdDraw.filledCircle(x, y, radio);
		radio = radio - 2;
		super.dibujar();
		radio = radio + 2;
	}
}
