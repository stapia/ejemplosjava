package pelotas4;

import stdlib.StdDraw;

public class PelotaCuadrada extends Pelota {
	
	private static final double RADIO_MINIMO = 3.0;
	private static final double RADIO_MAXIMO = 7.0;
	private static final double INCREMENTO_RADIO = 1.0;
	private static final int PERIODO_INCREMENTO = 400;

	int contador = 1;

	public PelotaCuadrada(double x, double y) {
		this.x = x;
		this.y = y;
		radio = RADIO_MINIMO;
		color = StdDraw.DARK_GRAY;
	}

	@Override
	public void tick() {
		if ( contador % PERIODO_INCREMENTO == 0 ) {
			radio += INCREMENTO_RADIO;
			if ( radio > RADIO_MAXIMO ) radio = RADIO_MINIMO;
		}
		++contador;
	}

	@Override
	public void reset() {
		// No hace nada
	}

	@Override
	public void accion() {
		// No hace nada
	}
	
	@Override
	public boolean esUnPuntoInterno(double x, double y) {
		return Math.abs(this.x-x) <= radio && Math.abs(this.y-y) <= radio;  
	}

	/**
	 * Dibuja esta pelota cuadrada como un cuadrado con relleno.
	 * 
	 * Sustituye la implementación en la clase base. 
	 */
	@Override
	public void dibujar() {
		StdDraw.setPenColor(color);
		StdDraw.filledSquare(x, y, radio);
	}

}
