
import pelotas2.*;
import stdlib.StdDraw;

public class Principal {
	
	public static void main(String[] args) {
		Campo campo = new Campo();
		StdDraw.setScale(0, Campo.DIMENSION);
		// Campo.DIMENSION = 50; No puedo
		StdDraw.enableDoubleBuffering();
		
		while (true) {
			campo.tick();
			StdDraw.clear();
			campo.dibujar();
			StdDraw.show();
			StdDraw.pause(2);
			if ( campo.contador % 500 == 1) {
				campo.addPelota(new PelotaAzul(0,0));
			}			
		}
	}
}
