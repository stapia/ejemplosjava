package pelotas2;

import stdlib.StdDraw;

public class PelotaAzul implements Pelota {
	
	private double x;
	private double y;
	
	private double vx;
	private double vy;
	
	private double radio;
	
	public PelotaAzul(double x, double y) {
		this.x = x;
		this.y = y;
		vx = 2;
		vy = 1;
		radio = 1.5;
	}
	
	public void crece() {
		if ( radio < 4 ) 
			radio += 1.0;
		else 
			radio = 1.5;
	}
	
	@Override
	public void mover(double deltaTiempo) {
		x = x + vx * deltaTiempo;
		y = y + vy * deltaTiempo;
		if ( x >= Campo.DIMENSION || x <= 0) {
			vx = -vx;
		}
		if ( y > Campo.DIMENSION || y <= 0 ) {
			vy = -vy;
		}
	}
	
	
	@Override
	public void dibujar() {
		StdDraw.setPenColor(StdDraw.BLUE);
		StdDraw.filledCircle(x, y, radio);
	}

	
	@Override
	public double[] posicion() {
		double[] r = { x, y };
		return r;
	}

}
