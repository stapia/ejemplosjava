package pelotas2;

public interface Pelota {
	
	void mover(double deltaTiempo);
	
	void dibujar();
	
	double[] posicion();
}
