
public class Monomio {
	
	/* Atributos*/  
	double coeficiente;
	int exponente;
	
	/* Constructor antes por defecto y sin escribir */
	Monomio() {
		coeficiente = 0;
		exponente = 0;
	}	
	
	/* M�todo Constructor */
	Monomio(double c, int e) {
		coeficiente = c;
		if ( e < 0 ) {
			System.out.println("error");
		}
		exponente = e;
	}
	
	/* M�todo de clase.
	 * Toma dos par�metros de tipo Mnomio
	 * Multiplica y devuelve el resultado */
	static Monomio multiplicar(Monomio m1, Monomio m2) {
		double coef = m1.coeficiente * m2.coeficiente;
		int exp = m1.exponente + m2.exponente;
		
		Monomio resultado = new Monomio();
		resultado.coeficiente = coef;
		resultado.exponente = exp;
		return resultado;
	}
	
	/* M�todo de instancia (instancia es sinonimo de objeto)
	 * Falta un parametro? Pues no, ese par�metro es this 
	 * */
	Monomio multiplicar(Monomio m2) { 
		Monomio resultado = new Monomio();
		resultado.coeficiente = this.coeficiente * m2.coeficiente;
		resultado.exponente = this.exponente + m2.exponente;
		return resultado;		
	}
	
	public String toString() {
		return "Monomio " + coeficiente + " * x^" + exponente;
	}
	
	public static void main(String[] args) {
		Monomio a = new Monomio();
		Monomio b = new Monomio(2.2,2);
		
		a.coeficiente = 1.7;
		a.exponente = 7;
		
		Monomio m = Monomio.multiplicar(a, b);
		System.out.println("Monomio " + m.coeficiente + " * x^" + m.exponente);

		a = Monomio.multiplicar(b, m);
		System.out.println("Monomio " + a.coeficiente + " * x^" + a.exponente);

		m = a.multiplicar(b);
		System.out.println(m);
	}

}
