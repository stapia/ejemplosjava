package inmutable;

/**
 * 
 * @author stapia
 *
 */
public class OtroNumero {
	private int num;
	public OtroNumero(int num) {
		this.num = num;
	}
	int getNum() {
		return num;
	}
	
	/**
	 * El m�todo add suma a "este" objeto
	 * el valor del parametro a y devuelve
	 * un nuevo objeto
	 * @param a El argumento que se va a sumar
	 * @return Un nuevo Numero con el resultado
	 */
	Numero add(int a) {
		return new Numero(num+a);
	}
	
	public static void main(String[] args) {
		Numero a = new Numero(4);
		Numero r = a.add(1);
	}
}
