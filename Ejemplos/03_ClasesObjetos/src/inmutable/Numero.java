package inmutable;

public class Numero {
	
	/* No se puede asignar */
	private final int num;
	
	public Numero(int num) {
		/* S� se puede inicializar (en el constructor) */ 
		this.num = num;
	}
	
	/* Observador */
	int getNum() {
		return num;
	}
	
	Numero add(int a) {
		return new Numero(num+a);
	}
}

