package numero;

public class Numero {
	
	private int num;
	
	public Numero() {
		num = 1;
	}
	
	/* Modificador */ 
	void add(int a) {
		if ( num + a > 0 ) {
			num += a;
		}
	}
	
	/* Observador */
	int getNum() {
		return num;
	}
}

