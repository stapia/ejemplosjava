package numero;

public class Par {
	
	final Numero n1;
	Numero n2;
	
	Par(int a, int b) {
		n1 = new Numero();
		n1.add(a);
		n2 = new Numero();
		n2.add(b);
	}
	
	/* Est� prohibido "cambiar" de objeto, 
	 * es decir asignarlo.
	void cambiarN1(int a) {
		n1 = new Numero();
		n1.add(a);
	}
	*/

	/* Pero no est� prohibido modificarlo */
	void addN1(int a) {
		n1.add(a);
	}

	/* Este se puede asignar y modificar */
	void cambiarN2(int b) {
		n2 = new Numero();
		n2.add(b);		
	}

}
