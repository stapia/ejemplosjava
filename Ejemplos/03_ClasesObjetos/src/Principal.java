import numero.Numero;

public class Principal {
	
	public static void main(String[] args) {
		Numero num = new Numero();
		System.out.println(num);
		/* No se puede porque la visibilidad 
		 * es "paquete" */
		// System.out.println(num.getNum());
	}

}
