import stdlib.StdDraw;

public class DibujaCuadrados {
	public static void main(String[] args) {
		StdDraw.setScale(0, 100);
		StdDraw.setPenColor(StdDraw.LIGHT_GRAY);
		StdDraw.filledSquare(50, 50, 10);
		StdDraw.setPenRadius(0.02);
		StdDraw.setPenColor(StdDraw.BLUE);
		StdDraw.point(50, 50);
		StdDraw.point(60, 60);
	}

}
