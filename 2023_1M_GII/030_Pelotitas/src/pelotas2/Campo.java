package pelotas2;

import java.util.Random;

public class Campo {
	
	public static final Random r = new Random();
	
	private static final double DELTA_TIEMPO = 0.005;
	public static final double DIMENSION = 100.0;
	
	private Pelota[] pelotas;
	int contador;
	
	public Campo() {
		pelotas = new Pelota[6];
		pelotas[0] = new PelotaRoja(10,80);
		pelotas[1] = new PelotaRoja(50,50);
		pelotas[2] = new PelotaRoja(40,10);
		pelotas[3] = new PelotaAzul(15,80);
		pelotas[4] = new PelotaAzul(55,50);
		pelotas[5] = new PelotaAzul(45,10);
		contador = 0;
	}
	
	public void tick() {
		for ( int i = 0; i < pelotas.length; ++i ) {
			pelotas[i].mover(DELTA_TIEMPO);
		}	
		if ( contador % 500 == 1) {
			PelotaRoja.vx = 1 + r.nextInt(10);
			PelotaRoja.vy = 1 + r.nextInt(10);
		}
		++contador;
	}
	
	public void dibujar() {
		for ( int i = 0; i < pelotas.length; ++i ) {
			pelotas[i].dibujar();
		}
	}

}
