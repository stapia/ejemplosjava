package pelotas2;

import stdlib.StdDraw;

public class PelotaAzul implements Pelota {
	
	private double x;
	private double y;
	
	private double vx;
	private double vy;
	
	public PelotaAzul(double x, double y) {
		this.x = x;
		this.y = y;
		vx = 2;
		vy = 1;
	}
	
	@Override
	public void mover(double deltaTiempo) {
		x = x + vx * deltaTiempo;
		y = y + vy * deltaTiempo;
		if ( x >= Campo.DIMENSION || x <= 0) {
			vx = -vx;
		}
		if ( y > Campo.DIMENSION || y <= 0 ) {
			vy = -vy;
		}
	}
	
	
	@Override
	public void dibujar() {
		StdDraw.setPenColor(StdDraw.BLUE);
		StdDraw.filledCircle(x, y, 1.5);
	}

	
	@Override
	public double[] posicion() {
		double[] r = { x, y };
		return r;
	}

}
