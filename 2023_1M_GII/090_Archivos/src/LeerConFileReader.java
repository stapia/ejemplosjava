import java.io.*;
import java.util.*;

public class LeerConFileReader {
	
	public static void main(String[] args) {
		leerMatriz(args);
	}
	
	public static void leerMatriz(String[] args) {
		try (Scanner r = new Scanner(new File("dato.txt"))) {
			int[][] matriz = new int[2][];
			int i = 0;
			while ( r.hasNextInt() ) {
				int tam = r.nextInt();
				System.out.println("tam: " + tam);
				matriz[i] = new int[tam];
				for ( int j = 0; j < tam; ++j ) {
					int numero = r.nextInt();
					matriz[i][j] = numero;
				}
				++i;
			}
			System.out.println(matriz[0][1]);
			System.out.println(Arrays.toString(matriz[0]));
			System.out.println(Arrays.toString(matriz[1]));
			System.out.println();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}			
	}
	
	public static void conScanner(String[] args) {
		try (Scanner r = new Scanner(new File("palabra.txt"))) {
			while ( r.hasNextLine() ) {
				String palabra = r.nextLine();
				System.out.println(palabra);
			}
			System.out.println();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}	
	}
	
	public static void conTryWithResources(String[] args) {
		try (FileReader r = new FileReader(new File("Hola.txt"))) {
			int dato = r.read();
			while ( dato != -1 ) {
				System.out.print(dato);
				System.out.print(" ");
				dato = r.read();
			}
			System.out.println();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void clasico(String[] args) {
		File f = new File("Hola.txt");
		FileReader r = null;
		try {
			r = new FileReader(f);
			int dato = r.read();
			System.out.println(dato);
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el archivo");
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Error al leer");
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (r != null) r.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
