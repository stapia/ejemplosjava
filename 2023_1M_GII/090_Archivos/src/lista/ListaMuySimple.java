package lista;

public class ListaMuySimple<E> implements IListaSimple<E> {
	
	Nodo<E> primero;
	Nodo<E> ultimo;
	
	@Override
	public void insertAtEnd(E e) {
		if ( primero == null ) { // Esta vacia
			primero = new Nodo<>(e, null);
			ultimo = primero;
		} else { // No esta vacia
			Nodo<E> aux = new Nodo<>(e, null);
			ultimo.next = aux;
			ultimo = aux;
		}
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[ ");
		Nodo<E> aux = primero;
		while ( aux != null ) {
			String str = aux.e.toString();
			builder.append(str);
			builder.append(" ");
			aux = aux.next;
		}
		builder.append(" ]\n");
		return builder.toString();
	}
}

class Nodo<E> {
	E e;
	Nodo<E> next;
	
	public Nodo(E e, Nodo<E> next) {
		this.e = e;
		this.next = next;
	}
}
