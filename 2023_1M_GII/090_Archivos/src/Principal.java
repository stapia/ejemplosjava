import java.io.*;
import java.util.*;

import lista.*;

public class Principal {
	
	public static void main(String[] args) {
		ListaMuySimple<IListaSimple<Integer>> lista = new ListaMuySimple<>();
		leerArchivo(lista);
		System.out.println(lista);
	}
	
	public static ListaMuySimple<Integer> crearFila(String linea) {
		ListaMuySimple<Integer> listaInt = new ListaMuySimple<>();
		String[] numeros = linea.split(" ");
		for (int i = 0; i < numeros.length; ++i ) {
			int num = Integer.parseInt(numeros[i]);
			listaInt.insertAtEnd(num);
		}
		return listaInt;
	}
	
	public static void leerArchivo(IListaSimple<IListaSimple<Integer>> lista) {
		try (Scanner r = new Scanner(new File("datos.txt"))) {
			while ( r.hasNextLine() ) {
				String linea = r.nextLine();
				lista.insertAtEnd(crearFila(linea));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
