package dibujar;

import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

public class VentanaKeys implements KeyListener, ActionListener {

	private JFrame frame = new JFrame();
	private Lienzo lienzo;
	private JLabel label;
	private Movil movil = new Movil();
	private ArrayList<Volante> volantes = new ArrayList<>();
	Timer timer;
	private int delay = 100;
	
	public VentanaKeys() {
		timer = new Timer(delay , this);
		timer.start();
		frame.addKeyListener(this);
		
		frame.setLayout(null);
		frame.setSize(1440, 870);
		
		lienzo = new Lienzo();
		lienzo.add(movil);
		lienzo.setBounds(10, 40, 1400, 700);
		frame.add(lienzo);
		
		for ( int i = 0; i < 10; ++i ) {
			Volante v = new Volante();
			volantes.add(v);
			lienzo.add(v);
		}
		
		label = new JLabel("Hola mundo");
		label.setBounds(10, 10, 1400, 35);
		frame.add(label);
		
		frame.setVisible(true);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		label.setText(e.toString());
		if ( e.getKeyChar() == 'a' ) {
			movil.mover(-20);
		} else if ( e.getKeyChar() == 'z' ) {
			movil.mover(+20);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		for ( Volante v : volantes ) {
			v.mover();
			v.colisiona(movil);
		}
		label.setText("Vida: " + movil.vida());
	}
}
