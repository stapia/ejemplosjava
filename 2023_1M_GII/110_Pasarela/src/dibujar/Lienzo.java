package dibujar;


import javax.swing.*;
import javax.swing.Timer;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

@SuppressWarnings("serial")
public class Lienzo extends JComponent implements ActionListener {

	int width = 800; 
	int height = 550;
	Color color = Color.black;
	final int deltaTime = 100;
	Timer timer;
	
	private ArrayList<IDibujar> dibujables = new ArrayList<>();
	
	public Lienzo() {
		 timer = new Timer(deltaTime, this);
		 timer.start();
	}
	
	public void add(IDibujar d) {
		dibujables.add(d);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		g.setColor(color);
		g.fillRect(0, 0, width, height);
		
		g.setColor(Color.white);
		for ( IDibujar d : dibujables ) {
			d.dibujar(g);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		repaint();
	}	
}
