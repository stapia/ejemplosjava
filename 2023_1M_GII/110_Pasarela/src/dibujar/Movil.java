package dibujar;

import java.awt.Color;
import java.awt.Graphics;

public class Movil implements IDibujar {
	
	public static final int DIMENSION = 20;
	private final int x = 30;
	private int y = 50;
	private int vida = 100;
	
	public void mover(int inc) {
		y += inc;
	}

	@Override
	public void dibujar(Graphics g) {
		g.setColor(Color.BLUE);
		g.fill3DRect(x, y, DIMENSION, DIMENSION, true);
	}
	
	public void descontarVida() {
		vida -= 10;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int vida() {
		return vida;
	}
}
