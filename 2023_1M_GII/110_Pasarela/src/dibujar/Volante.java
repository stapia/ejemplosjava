package dibujar;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class Volante implements IDibujar {
	
	public static final int DIMENSION = 20;
	int x;
	int y;
	
	private static final Random r = new Random();
	
	public Volante() {
		x = 770 + r.nextInt(200);
		y = r.nextInt(550);
	}
	
	public void mover() {
		x -= 5;
		if ( x < 0 ) {
			x = 770;
			y = r.nextInt(550);
		}
	}
	
	@Override
	public void dibujar(Graphics g) {
		g.setColor(Color.RED);
		g.fillOval(x, y, DIMENSION, DIMENSION);
	}

	public void colisiona(Movil m) {
		if ( hayColision(m) ) {
			m.descontarVida();
		}
	}
	
	private boolean hayColision(Movil m) {
		int xc = x + DIMENSION / 2;
		int yc = y + DIMENSION / 2;
		int yt = m.getY() - DIMENSION / 2;
		int yb = m.getY() + Movil.DIMENSION + DIMENSION / 2;
		int xl = m.getX() - DIMENSION / 2;
		int xr = m.getX() + Movil.DIMENSION + DIMENSION / 2;
		return ( xc < xr) && ( xc > xl ) && ( yc > yt ) && ( yc < yb); 
	}
}
