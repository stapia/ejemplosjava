package dibujar;


import java.awt.Graphics;

public interface IDibujar {
	
	void dibujar(Graphics g);

}
