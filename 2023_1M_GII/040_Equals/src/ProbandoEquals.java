
public class ProbandoEquals {
	
	public static void main(String[] args) {
		Numero a = new Numero();
		a.numero = 3;
		Numero b = new Numero();
		b.numero = 3;
		Numero c = new Numero();
		c.numero = 4;
		Numero d = a;
		System.out.println("a == b: " + (a == b));
		System.out.println("a == d: " + (a == d));
		System.out.println("a.equals(b): " + a.equals(b));
		System.out.println("a.equals(c): " + a.equals(c));
		
		String cadena = "Hola";
		System.out.println("a.equals(cadena): " + a.equals(cadena));
		
		String otra = "Hola";
		System.out.println("(otra == cadena): " + (otra == cadena));
		System.out.println("otra.equals(cadena): " + otra.equals(cadena));
		
		String distinta = new String("Hola");
		System.out.println("(distinta == cadena): " + (distinta == cadena));
		System.out.println("distinta.equals(cadena): " + distinta.equals(cadena));
	}

}
