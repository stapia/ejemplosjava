
public class Numero {
	
	protected int numero;
	
	@Override
	public boolean equals(Object otro) {
		if ( otro instanceof Numero ) {
			Numero o = (Numero)otro;
			return this.numero == o.numero;
		}
		return false;
	}

}
