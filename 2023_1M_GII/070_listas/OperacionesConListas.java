public class OperacionesConListas {
    public static void main(String[] args) {
      ListaElemento lis = new ListaElemento();
      lis.insertarDelante(7);
      lis.insertarDelante(3);
      Elemento a = lis.prim;
      lis.insertarDetras(a, 20);
      int sumando = lis.suma();
      Elemento b1 = lis.buscarUltimo();
      Elemento b2 = lis.buscarPar();
      lis.eliminarPrimero();
      lis.insertarDelante(99);
      lis.eliminarDetras(b2);
    }
}

class Elemento {
  int dato;
  Elemento sig;
}

class ListaElemento {
  Elemento prim;
  
  void insertarDelante(int num) {
    Elemento aux = new Elemento(); // a
    aux.dato = num; // b
    aux.sig = prim; //c
    prim = aux; // d
  }
  
  void insertarDetras(Elemento anterior, int num) {
    Elemento aux = new Elemento(); // a
    aux.dato = num; // b
    aux.sig = anterior.sig; // c
    anterior.sig = aux; //d
  }
  
  int suma() {
    int s = 0;
    Elemento aux = prim;
    while ( aux != null ) {
      s += aux.dato; // Esto hace cosas
      aux = aux.sig; // Esto avanza
    }
    return s;
  }
  
  Elemento buscarUltimo() {
    Elemento aux = prim;
    while ( aux.sig != null ) {
      aux = aux.sig; // Esto avanza
    }
    return aux;
  }
  
  Elemento buscarPar() {
    Elemento aux = prim;
    while ( aux != null && aux.dato % 2 != 0 ) { // Lazy Evaluation
      aux = aux.sig; // Esto avanza
    }
    return aux;
  }
  
  void eliminarPrimero() {
    prim = prim.sig;
  }
  
  void eliminarDetras(Elemento anterior) {
    anterior.sig = anterior.sig.sig;
  }
  
}