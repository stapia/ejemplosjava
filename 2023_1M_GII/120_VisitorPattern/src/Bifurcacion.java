
// * Es una clase que tiene un Nodo Horizontal
// y otro vertical. 
// * E implementa INodo
// * Implementa un constructor "conveniente"
// * Cuando la Bifurcacion acepta un visitor
// Primero devuelve la llamada y luego hace
// que se visite el horizontal y luego el vertical
public class Bifurcacion implements INodo {
	private Horizontal horizontal;
	private INodo vertical;
	
	// Impedir que se construya el objeto cuando h sea null
	public Bifurcacion(Horizontal h, INodo v) {
		// Se debe lanzar una excepci�n:
		if ( h == null ) {
			throw new IllegalArgumentException("El objeto en horizontal no puede ser null");
		}
		horizontal = h;
		vertical = v;
	}
	
	@Override
	public void accept(IVisitor v) {
		v.visit(this);
		horizontal.accept(v);
		if ( vertical != null ) {
			vertical.accept(v);			
		}
	}
}
