import java.util.Arrays;

public class Horizontal implements INodo {
	
	private int[] vector;
	
	public Horizontal(int[] vector) {
		this.vector = Arrays.copyOf(vector, 4);
	}
	
	public int get(int i) {
		return vector[i];
	}

	@Override
	public void accept(IVisitor v) {
		v.visit(this);
	}
}
