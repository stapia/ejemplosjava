
public class Main {
	
	public static void main(String[] args) {
		int[] vh1 = { 7, 4, 2, 1 };
		int[] vv1 = { 3, 2, 5 };
		int[] vv2 = { 1, 20, 6 };
		int[] vh2 = { 8, 2, 33, 15};
		int[] vh3 = { 1, 0, 0, 0};
		Horizontal h3 = new Horizontal(vh3);
		Bifurcacion b3 = new Bifurcacion(h3, null);
		Horizontal h2 = new Horizontal(vh2);
		Bifurcacion b2 = new Bifurcacion(h2, b3);
		Vertical v2 = new Vertical(vv2, b2);
		Vertical v1 = new Vertical(vv1, v2);
		Horizontal h1 = new Horizontal(vh1);
		Bifurcacion b1 = new Bifurcacion(h1, v1);
		PrintVisitor print = new PrintVisitor();
		b1.accept(print);
		
		SumaVisitor sumar = new SumaVisitor();
		b1.accept(sumar);
		System.out.println(sumar);
		
		SumaAlternadaVisitor sumarA = new SumaAlternadaVisitor();
		b1.accept(sumarA);
		System.out.println(sumarA);
		
	}

}
