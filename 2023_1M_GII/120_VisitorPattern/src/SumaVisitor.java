
public class SumaVisitor implements IVisitor {
	
	int sumaH = 0;
	int sumaV = 0;

	@Override
	public void visit(Bifurcacion b) {
		return;
	}

	@Override
	public void visit(Horizontal h) {
		for ( int i = 0; i < 4; ++i ) {
			sumaH += h.get(i);
		}		
	}

	@Override
	public void visit(Vertical v) {
		for ( int num : v.get() ) {
			sumaV += num;
		}
	}
	
	@Override
	public String toString() {
		return String.format("Suma horizontal: %d; Suma vertical: %d", sumaH, sumaV);
	}

}
