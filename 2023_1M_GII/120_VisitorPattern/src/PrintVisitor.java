import java.util.Arrays;

public class PrintVisitor implements IVisitor {

	@Override
	public void visit(Bifurcacion b) {
		System.out.print("* -> ");
	}

	@Override
	public void visit(Horizontal h) {
		for ( int i = 0; i < 4; ++i ) {
			System.out.print(" " + h.get(i));
		}
		System.out.println();
	}

	@Override
	public void visit(Vertical v) {
		System.out.println("|");
		for ( int num : v.get() ) {
			System.out.println(num);
		}
	}

}
