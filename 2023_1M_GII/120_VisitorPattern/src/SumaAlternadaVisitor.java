
public class SumaAlternadaVisitor implements IVisitor {
	
	int suma = 0;
	boolean positivo = false;

	@Override
	public void visit(Bifurcacion b) {
		positivo = !positivo; // Se alterna al llamar aqu�. Se llama antes de sumar el horizontal
	}

	@Override
	public void visit(Horizontal h) {
		for ( int i = 0; i < 4; ++i ) {
			suma += positivo ? h.get(i) : -h.get(i);
		}		
	}

	@Override
	public void visit(Vertical v) {
		for ( int num : v.get() ) {
			suma += num;
		}
	}
	
	@Override
	public String toString() {
		return String.format("Suma alternada: %d", suma);
	}

}
