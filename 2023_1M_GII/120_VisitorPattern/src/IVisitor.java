
public interface IVisitor {
	
	void visit(Bifurcacion b);
	void visit(Horizontal h);
	void visit(Vertical v);
	
}
