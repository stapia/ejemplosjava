
// Un vector de 3 n�meros,
// un constructor conveniente
// una manera de acceder a los 3 n�meros
// Tiene que implementar INodo
// Puede tener "debajo" otro Nodo

public class Vertical implements INodo {
	
	int[] vector = new int[3];
	INodo next;
	
	public Vertical(int[] v, INodo n) {
		vector = v; // No copio el vector porque no es inmutable
		next = n;
	}
	
	public int[] get() {
		return vector;
	}

	@Override
	public void accept(IVisitor v) {
		v.visit(this);
		next.accept(v);
	}
}
