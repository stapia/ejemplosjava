package algebra;

public class PruebasConGestionadas {
	
	public static void main(String[] args) {
		Vector v1 = new Vector(2,-5,2);
		Vector v2 = new Vector(3,1);
		try {
			Vector suma = v1.sumar(v2);
			System.out.println(suma);
		} catch (VectorDimException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

}
