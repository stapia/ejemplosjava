package algebra;

public class Pruebas {
	
	public static void main(String[] args) {
		Vector v1 = new Vector(2,-5,2);
		Vector v2 = new Vector(3,1);
		try {
			double ps = Vector.productoEscalar(v1, v2);
			System.out.println(ps);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Algo ha ido mal...");
		}
		System.out.println("Despu�s de catch");
		try {
			double ps = Vector.productoEscalar(v1, v2);
			System.out.println(ps);
		} catch (IndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
			System.out.println("Me quedo en IndexOutOfBoundsException");
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			System.out.println("Me quedo en IllegalArgumentException");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("LLego a Exception");
		}
		System.out.println("Despu�s de catch multiple");

		double ps = Vector.productoEscalar(v1, v2);
		System.out.println(ps);
		System.out.println("Final de main");
	}

}
