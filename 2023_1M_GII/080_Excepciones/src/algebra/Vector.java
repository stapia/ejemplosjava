package algebra;

import java.util.Arrays;

public class Vector {
	
	private double[] v;
	
	public Vector(double a, double b) {
		v = new double[2];
		v[0] = a;
		v[1] = b;
	}
	
	public Vector(double a, double b, double c) {
		v = new double[3];
		v[0] = a;
		v[1] = b;
		v[2] = b;
	}
	
	// PRECONDICION: el v1 y v2 deben tener la misma dim
	public static double productoEscalar(Vector v1, Vector v2) {
		if ( v1.v.length != v2.v.length ) {
			throw new IllegalArgumentException("v1 y v2 deben tener la misma dim");
		}
		double suma = 0;
		for ( int i = 0; i < v1.v.length; ++i) {
			suma += v1.v[i] * v2.v[i];
		}
		return suma;
	}
	
	// Retorna un *nuevo* objeto Vector con el resultado de
	// la suma.
	// PRECONDICION: el this y v2 deben tener la misma dim
	public Vector sumar(Vector v2) throws VectorDimException {
		if ( this.v.length != v2.v.length ) {
			throw new VectorDimException();
		}
		double[] resultado = new double[this.v.length];
		for ( int i = 0; i < v.length; ++i ) {
			resultado[i] = v[i] + v2.v[i];
		}
		if ( v.length == 2 ) {
			return new Vector(resultado[0], resultado[1]);
		} else {
			return new Vector(resultado[0], resultado[1], resultado[2]);
		}
	}
	public String toString() {
		return Arrays.toString(v);
	}

}
