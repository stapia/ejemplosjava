
import java.util.Scanner;

import rebote.*;
import stdlib.StdDraw;

public class Principal {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Campo campo = new Campo(sc);
		
		StdDraw.setScale(0, Campo.DIMENSION);
		// Campo.DIMENSION = 50; No puedo
		StdDraw.enableDoubleBuffering();
		
		while (true) {
			//campo.tick();
			StdDraw.clear();
			campo.dibujar();
			StdDraw.show();
			StdDraw.pause(2);
		}
	}
}
