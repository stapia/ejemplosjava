package rebote;

import java.util.Scanner;

import stdlib.StdDraw;

public class Obstaculo {
	
	private static final double RADIO = 0.01;
	protected double x;
	protected double y;
	private double ancho;
	protected double alto;
	
	public Obstaculo(double ix, double iy) {
		x = ix;
		y = iy;
		ancho = 3;
		alto = 3;
	}
	
	public Obstaculo() {
		ancho = 3;
		alto = 3;
	}
	
	public void read(Scanner sc) {
		x = sc.nextInt();
		y = sc.nextInt();
	}
	
	public void dibujar() {
		StdDraw.setPenRadius(RADIO);
		StdDraw.line(x, y, x + ancho, y);
		StdDraw.line(x + ancho, y, x + ancho, y - alto);
		StdDraw.line(x + ancho, y - alto, x, y - alto);
		StdDraw.line(x, y - alto, x, y);
	}


}
