package rebote;

public class Factoria {

	public static Obstaculo crear(String nombreObstaculo) {
		if( nombreObstaculo.equals("Obstaculo") ) {
			return new Obstaculo();
		} 

		if ( nombreObstaculo.equals("ObstaculoRectangular")) {
			return new ObstaculoRectangular();
		}
		
		if ( nombreObstaculo.equals("ObstaculoCircular")) {
			return new ObstaculoCircular();
		}
		return null;
	}

}
