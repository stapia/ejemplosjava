package rebote;

import java.util.Random;
import java.util.Scanner;

import stdlib.StdDraw;
import java.awt.Color;

public class Campo {
	
	private static final double DELTA_TIEMPO = 0.05;
	public static final Random r = new Random();
	public static final double DIMENSION = 100.0;
	
	private Obstaculo[] obstaculos;
	int contador;
	
	public Campo() {
		obstaculos = new Obstaculo[6];
		obstaculos[0] = new Obstaculo(10,80);
		obstaculos[1] = new ObstaculoRectangular(80,30);
		obstaculos[2] = new Obstaculo(40,10);
		obstaculos[3] = new Obstaculo(15,80);
		obstaculos[4] = new ObstaculoCircular();
		obstaculos[5] = new ObstaculoCircular();
	}
	
	public Campo(Scanner sc) {
		int tam = sc.nextInt();
		obstaculos = new Obstaculo[tam];
		for ( int i = 0; i < tam; ++i ) {
			String nombreObstaculo = sc.next();
			obstaculos[i] = Factoria.crear(nombreObstaculo);
			obstaculos[i].read(sc);
		}
	}
	
	public void dibujar() {
		for ( int i = 0; i < obstaculos.length; ++i ) {
			// obstaculos[i].cambiarColor(StdDraw.BLUE); NO puedo
			obstaculos[i].dibujar();
		}
	}
}
