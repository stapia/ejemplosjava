
public class Camiseta {
	
	enum Tipo { PICO, REDONDA }
	
	int talla;
	Tipo tipo;
	
	void setTipo(String str) {
		tipo = Tipo.valueOf(str);
	}
	
	boolean pegaCon(String prenda) {
		return tipo == Tipo.PICO && prenda.equalsIgnoreCase("camisa");
	}
	
	@Override
	public String toString() {
		return String.format("talla: %d, tipo: %s", talla, tipo);
	}

}
