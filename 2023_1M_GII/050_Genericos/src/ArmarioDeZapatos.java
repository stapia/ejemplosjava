
public class ArmarioDeZapatos {
	
	Zapato cajon1;
	Zapato cajon2;
	
	public void guardar(Zapato z) {
		if ( cajon1 == null ) {
			cajon1 = z;
			return;
		}
		if ( cajon2 == null ) {
			cajon2 = z;
			return;
		}
		System.err.println("No cabe");
	}
	
	public Zapato sacar() {
		Zapato result = null;
		if ( cajon1 != null ) {
			result = cajon1;
			cajon1 = null;
		} else if ( cajon2 != null ) {
			result = cajon2;
			cajon2 = null;
		} else {
			System.err.println("No hay zapatos");
		}
		return result;
	}
}
