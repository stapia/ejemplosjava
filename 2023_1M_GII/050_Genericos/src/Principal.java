
public class Principal {
	
	public static void main(String[] args) {
		Camiseta c1 = new Camiseta();
		c1.talla = 37;
		c1.tipo = Camiseta.Tipo.PICO;
		System.out.println("Camiseta c1: " + c1);
		
		ArmarioDeCamisetas arma1 = new ArmarioDeCamisetas();
		arma1.guardar(c1);
		System.out.println(arma1);
		
		Camiseta fuera = arma1.sacar();
		System.out.println("Camiseta fuera: " + fuera);
		System.out.println(arma1);
		
		System.out.println("===== Con generico ==== ");
		// Tambi�n vale: ArmarioDe<Camiseta> arma2 = new ArmarioDe<Camiseta>();
		ArmarioDe<Camiseta> arma2 = new ArmarioDe<>();
		String cadena = "Soy un impostor de camiseta";
		// arma2.guardar(cadena); No puedo arma2 es de camisetas
		arma2.guardar(c1);
		System.out.println(arma2);
		
		fuera = arma2.sacar();
		System.out.println("c1 y fuera son el mismo: " + (c1 == fuera) );
		System.out.println("Camiseta fuera: " + fuera);
		System.out.println(arma2);
		
		Camiseta c2 = new Camiseta();
		Camiseta c3 = new Camiseta();
		c3.talla = 34;
		arma2.guardar(c1);
		arma2.guardar(c2);
		System.out.println("Armario con 2 camisetas: " + arma2);
		arma2.guardar(c3);
		System.out.println("Armario con 3 camisetas??: " + arma2);
		
		ArmarioDe<String> armaStr = new ArmarioDe<>();
		armaStr.guardar(cadena); // Ahora s�
		
		System.out.println("c1 pega con camisa: " + c1.pegaCon("camisa"));
		System.out.println("c1 pega con jersey: " + c1.pegaCon("jersey"));
		
		c2.setTipo("REDONDA");
		// c3.setTipo("CUADRADA"); da ERROR
	}
}
