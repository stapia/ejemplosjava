
public class ArmarioDeCamisetas {
	
	Camiseta cajon1;
	Camiseta cajon2;
	
	public void guardar(Camiseta z) {
		if ( cajon1 == null ) {
			cajon1 = z;
			return;
		}
		if ( cajon2 == null ) {
			cajon2 = z;
			return;
		}
		System.err.println("No cabe");
	}
	
	public Camiseta sacar() {
		Camiseta result = null;
		if ( cajon1 != null ) {
			result = cajon1;
			cajon1 = null;
		} else if ( cajon2 != null ) {
			result = cajon2;
			cajon2 = null;
		} else {
			System.err.println("No hay camisetas");
		}
		return result;
	}
	
	@Override
	public String toString() {
		return String.format("c1: %s, c2: %s", cajon1, cajon2);
	}
}
