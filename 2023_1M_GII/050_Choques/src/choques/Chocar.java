package choques;

public interface Chocar {
	
	// Para obtener la coordenada x
	double x(); 
	// Para obtener la coordenada y
	double y();
	
	void colision(Figura otra);
	
	void reflejar(boolean enX, boolean enY);
	
	void cambiarVelocidad(boolean aumentar);

	void choqueEspecial();

}
