package choques;

import java.util.Scanner;
import stdlib.StdDraw;

public class Principal {
	
	private static final int PAUSE_TIME = 1; // milliseconds

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Campo campo = new Campo(sc);
		
		StdDraw.setScale(0, Campo.DIMENSION);
		StdDraw.enableDoubleBuffering();
		
		while (true) {
			campo.tick();
			StdDraw.clear();
			campo.dibujar();
			StdDraw.show();
			StdDraw.pause(PAUSE_TIME);
		}
	}
}
