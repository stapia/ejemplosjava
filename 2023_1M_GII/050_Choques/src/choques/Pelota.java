package choques;

import java.awt.*;
import stdlib.StdDraw;

public abstract class Pelota implements Figura, Chocar {
	
	private static final double DELTA_TIEMPO = 0.1;
	private static final double RADIUS = 1;
	double x;
	double y;
	
	double vx;
	double vy;
	
	Color color = StdDraw.BLACK;
	
	/* Choca contra otra figura */
	@Override
	public void colision(Figura otra) {
		otra.chocar(this);	
	}
	
	/* Se refleja al chocar */
	@Override
	public void reflejar(boolean enX, boolean enY) {
		if ( enX ) {
			vx = -vx;
		}
		if ( enY ) {
			vy = -vy;
		}
	}
	
	/* Cambia velocidad al chocar */
	@Override
	public void cambiarVelocidad(boolean aumentar) {
		if ( aumentar ) {
			vx = 1.2*vx;
		} else {
			vx = 0.8*vx;
		}
	}
	
	@Override
	public void dibujar() {
		StdDraw.filledCircle(x, y, RADIUS);
		
	}

	@Override
	public void tick() {
		x = x + vx * DELTA_TIEMPO;
		y = y + vy * DELTA_TIEMPO;
		if ( x >= Campo.DIMENSION || x <= 0) {
			vx = -vx;
		}
		if ( y > Campo.DIMENSION || y <= 0 ) {
			vy = -vy;
		}
	}
}
