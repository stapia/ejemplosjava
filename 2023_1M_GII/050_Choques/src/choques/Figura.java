package choques;

import java.util.*;

public interface Figura extends Dibujar {

	/* El estado evoluciona con el tiempo */
	void tick();

	/* Lee los datos de configuracion */
	void read(Scanner sc);

	/* (Una pelota) choca contra la figura */
	void chocar(Chocar pelota);

}
