package choques;

import java.util.*;

public class Campo {

	public static final double DIMENSION = 100;
	private Figura[] figuras;
	
	public Campo(Scanner sc) {
		int tam = sc.nextInt();
		figuras = new Figura[tam];
		for ( int i = 0; i < tam; ++i ) {
			String cosa = sc.next();
			figuras[i] = Factoria.crear(cosa);
			figuras[i].read(sc);
		}
	}

	public void dibujar() {
		for ( Figura f : figuras ) {
			f.dibujar();
		}
	}

	public void tick() {
		for ( Figura f: figuras ) {
			if ( f instanceof Pelota ) {
				Chocar p = (Chocar) f;
				for ( Figura g: figuras ) {
					if ( f != g ) {
						p.colision(g);
					}
				}				
			}
		}
		for ( Figura f : figuras ) {
			f.tick();
		}
	}
}
