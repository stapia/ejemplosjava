public class YourClassNameHere {
    public static void main(String[] args) {
      Entero e1 = new Entero();
      Entero ult = e1;
      ult.num = 3;
      
      ult.e = new Entero();
      ult.e.num = 88;
      ult = ult.e;
      
      ult.e = new Entero();
      ult.e.num = 20;
      ult = ult.e;
      
      ult.e = new Entero();
      ult.e.num = 34;
      ult = ult.e;
    }
}

class Entero {
  int num;
  Entero e;
  
  public String toString() {
    return String.format("num: %d", num);
  }
}
