public class YourClassNameHere {
2	    public static void main(String[] args) {
3	      Entero e1 = new Entero();
4	      e1.num = 3;
5	      
6	      Entero e2 = new Entero();
7	      e2.num = 33;
8	      
9	      e1 = e2;
10	      
11	      e1 = new Entero();
12	      e1.num = 33;
13	      
14	      System.out.println(e1);
15	      System.out.println(e2);
16	    }
17	}
18	
19	class Entero {
20	  int num;
21	  
22	  public String toString() {
23	    return String.format("num: %d", num);
24	  }
25	}