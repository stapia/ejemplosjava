public class YourClassNameHere {
2	    public static void main(String[] args) {
3	      Entero[] array = new Entero[3];
4	      array[0] = new Entero();
5	      array[1] = new Entero();
6	      array[2] = new Entero();
7	      array[1].num = 4;
8	    }
9	}
10	
11	class Entero {
12	  int num;
13	  
14	  public String toString() {
15	    return String.format("num: %d", num);
16	  }
17	}