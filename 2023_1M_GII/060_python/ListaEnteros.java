public class YourClassNameHere {
    public static void main(String[] args) {
      
      Entero e1 = new Entero();
      e1.num = 3;
      e1.e = new Entero();
      e1.e.num = 88;
      
      e1.e.e = new Entero();
      e1.e.e.num = 20;
    }
}

class Entero {
  int num;
  Entero e;
  
  public String toString() {
    return String.format("num: %d", num);
  }
}