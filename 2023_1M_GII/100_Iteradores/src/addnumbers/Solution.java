package addnumbers;

class Solution {
	
	ListNode head; 
	ListNode last;
	
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
    	ListNode aux1 = l1;
    	ListNode aux2 = l2;
    	head = null; 
    	last = null;
    	int acarreo = 0;
    	while ( aux1 != null && aux2 != null ) {
    		// Hacer algo
    		int sum = acarreo + aux1.val + aux2.val;
    		int digit = sum % 10;
			insertar(digit);
    		acarreo = sum - digit;
    		// Avanzar
    		aux1 = aux1.next;
    		aux2 = aux2.next;	
    	}
    	
    	while ( aux1 != null ) {
    		// Hacer algo
    		int sum = acarreo + aux1.val;
    		int digit = sum % 10;
    		insertar(digit);
    		acarreo = sum - digit;
    		// Avanzar
    		aux1 = aux1.next;
    	}
    	
    	while ( aux2 != null ) {
    		// Hacer algo
    		int sum = acarreo + aux2.val;
    		int digit = sum % 10;
    		insertar(digit);
    		acarreo = sum - digit;
    		// Avanzar
    		aux1 = aux1.next;
    		aux2 = aux2.next;
    	}
    	
    	if ( acarreo != 0 ) {
    		int digit = acarreo;
    		insertar(digit);
    	}
    	return head;
    }
    
    private void insertar(int digit) {
    	if ( head == null ) {
    		// Insertar cuando no hay nada
    		head = new ListNode(digit, null);
    		last = head;
    	} else {
    		// Insertar detras del ultimo
    		last.next = new ListNode(digit, null);
    		last = last.next;
    	}
    }
}

class ListNode {
	 int val;
	 ListNode next;
	 ListNode() {}
	 ListNode(int val) { this.val = val; }
	 ListNode(int val, ListNode next) { 
		 this.val = val; this.next = next; 
	 }
}
