package addnumbers;

public class ParametrosMetodos {
	
	public static void main(String[] args) {
		Integer num = new Integer(34);
		cambiarEntero(num);
		System.out.println(num);
		Entero entero = new Entero(34);
		cambiarElOtroEntero(entero);
		System.out.println(entero);
	}

	private static void cambiarElOtroEntero(Entero entero) {
		entero.modificar(22);
	}

	private static void cambiarEntero(Integer num) {
		num = 22;
	}
}

class Entero {
	private int num;
	public Entero(int num) {
		this.num = num;
	}
	
	public int numero() {
		return num;
	}
	
	public void modificar(int num) {
		this.num = num;
	}
	
	public String toString() {
		return "" + num;
	}
}
