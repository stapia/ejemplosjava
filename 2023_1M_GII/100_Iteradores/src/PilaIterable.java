import java.util.Iterator;

public class PilaIterable implements Iterable<Integer> {
	
	ListNode head;
	
	public void add(int x) {
		head = new ListNode(x, head);
	}
	
	@Override
	public Iterator<Integer> iterator() {
		return new IteradorPila(head);
	}
}

class IteradorPila implements Iterator<Integer> {
	
	ListNode aux;
	
	IteradorPila(ListNode head) {
		aux = head;
	}

	@Override
	public boolean hasNext() {
		return aux != null;
	}

	@Override
	public Integer next() {
		int x = aux.val;
		aux = aux.next;
		return x;
	}
}

class ListNode {
	 int val;
	 ListNode next;
	 ListNode() {}
	 ListNode(int val) { this.val = val; }
	 ListNode(int val, ListNode next) { 
		 this.val = val; this.next = next; 
	 }
}
