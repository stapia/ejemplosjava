import java.util.Iterator;

public class ForEach {
	
	public static void main(String[] args) {
		usaIterador5();
	}

	public static void usaIterador1() {
		for ( int i = 0; i < 10; ++i ) {
			int x = i * 3;
			System.out.println(x);
		}
	}
	
	public static void usaIterador2() {
		System.out.println("Con iterador");
		Iterable<Integer> m3 = new MultiplosDeTres();
		Iterator<Integer> ite = m3.iterator();
		while (ite.hasNext() ) {
			int x = ite.next();
			System.out.println(x);
		}
	}
	
	public static void usaIterador3() {
		System.out.println("Con for each");
		Iterable<Integer> m3 = new MultiplosDeTres();
		for ( int x : m3 ) {
			System.out.println(x);
		}
	}
	
	public static void usaIterador4() {
		System.out.println("Array con for each");
		int[] a = { 3, 4, 5, 6 };
		for ( int x : a ) {
			System.out.println(x);
		}
	}
	
	public static void usaIterador5() {
		System.out.println("Array con pila");
		PilaIterable a = new PilaIterable();
		a.add(4);
		a.add(5);
		a.add(87);
		for ( int x : a ) {
			System.out.println(x);
		}
	}
	

}
