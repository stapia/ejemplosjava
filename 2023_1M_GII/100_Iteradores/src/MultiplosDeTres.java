import java.util.Iterator;

public class MultiplosDeTres implements Iterable<Integer> {

	@Override
	public Iterator<Integer> iterator() {
		return new IteradorMultiplosDeTres();
	}
}

class IteradorMultiplosDeTres implements Iterator<Integer> {
	
	int i;
	
	IteradorMultiplosDeTres() {
		i = 0;
	}

	@Override
	public boolean hasNext() {
		return i < 10;
	}

	@Override
	public Integer next() {
		int aux = i * 3;
		i = i + 1;
		return aux;
	}
	
}
